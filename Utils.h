#ifndef _UTILS_H
#define _UTILS_H

#define  DD_MMM_YYYY       1
#define  MM_DD_YYYY_1      2  // m/d/yyyy (default)
#define  MMM_DD_YYYY       3  // Feb 01 2016 or Feb 01, 2016
#define  MMDDYY1           4  // mm/dd/yy
#define  MMDDYY2           5  // mmddyy
#define  YY2YYYY           6
#define  YYMMDD1           7  // yy/mm/dd
#define  YYMMDD2           8  // yymmdd
#define  YYYY_MM_DD        9  // yyyy-mm-dd
#define  MMDDYYYY          10
#define  MM_DD_YYYY_2      11 // MM/DD/YYYY 

int	 iTrim(char *pString, int iLen=0);
char   *myTrim(char *pString);
char   *myTrim(char *pString, int iLen);
char   *myLTrim(char *pString, int iLen=0);
char   *myBTrim(char *pString, int iLen=0);
char   *strUpper(char *pInStr, char *pOutStr, int iLen=0);
char   *dateConversion(char *pFromDate, char *pToDate, int iFromFmt, int iExpYear=0);
int    replStr(LPSTR pStr, LPCSTR pFind, LPCSTR pRepl);
int    replStrAll(LPSTR pStr, LPCSTR pFind, LPCSTR pRepl);

int    replUChar(unsigned char *pBuf, unsigned char cSrch, unsigned char cRepl, int iLen);
int    replChar(char *pBuf, unsigned char cSrch, char cRepl, int iLen);
int    replChar(char *pBuf, unsigned char cSrch, char cRepl);
int    replCharEx(char *pBuf, char cSrch, char cRepl, int iLen=0);
int    replCharEx(char *pBuf, char *pCharSet, char cRepl=32, int iLen=0, bool bRemBlank=true, bool bReplUnPrt=true);
void   replQuoteEx(char *pBuf, int iLen);
int    replNull(char *pBuf, char cRepl=32, int iLen=0);
int    replNonNum(char *pBuf, char cRepl=32, int iLen=0);

int    remChar(char *pBuf, char cSrch);
int    remChar(char *pBuf, char cSrch, int iBufLen);
int    remUChar(unsigned char *pBuf, unsigned char cSrch, int iBufLen);
void   remCharEx(char *pBuf, char *pCharSet);
int    remUnPrtChar(LPSTR pBuf);
void   remDupWords(LPSTR pStr);
void   remDupStr(LPSTR pStr, LPCSTR pDel);

long   replaceByte(LPCSTR pInfile, char bSrchChar, char bReplChar, int iRecSize);
long   replByte_EQ(LPCSTR pInfile, LPCSTR pOutfile, char bSrchChar, char bReplChar=32);
long   replByte_LE(LPCSTR pInfile, LPCSTR pOutfile, char bSrchChar, char bReplChar=32);
int    replEmbededChar(LPCSTR pInfile, LPCSTR pOutfile, char bSrchChar, char bReplChar, int iMode, int iRecLen=1900);
// Replace all unprintable char (<32 or >126)
int    replUnPrtChar(LPSTR pBuf, char cRepl=32, int iLen=0);
// Replace all unprintable char and '\'.  Drop single quote.
int    replSqlChar(LPSTR pBuf, char cRepl=32, int iLen=0);
// Replace all unprintable char and '\'
int    replBadChar(LPSTR pBuf, char cRepl, int iLen=0);
// Remove Json sensitive char: backslash, double quote
int remJSChar(LPSTR pBuf, int iLen=0);
// Remove vertical bar, backslash, double quote from owner name
int cleanOwner(LPSTR pBuf, int iLen=0);

void   blankPad(char *pBuf, int iLen);
void   blankPadz(char *pBuf, int iLen);
void   blankPad_RemCRLF(char *pBuf, int iLen);

int    blankRem(char *pBuf, int iLen=0);
int    blankRem(char *pBuf, char bChkChar, int iLen=0);
int    blankRem(CString& pStr);
int    quoteRem(char *pBuf, int iLen=0);        // returns number of quotes removed
int    findUnPrtChar(LPSTR pBuf, int iLen=0);   // returns index of unprintable char, 0 if ok
int    remExtraQuote(LPSTR pBuf);

int    isPrintable(char *pBuf, int iLen);       // returns index of unprintable char, iLen if ok
char   isChar(LPCSTR pBuf, int iLen);           // return char between 'A'-'Z', blank if not
bool   isNumber(LPSTR pNumber);
bool   isNumber(LPSTR pNumber, int iLen);
bool   isBlank(LPCSTR pStr, int iLen=0);
bool   isTextFile(LPCSTR pInfile, int iLineLen);
bool   isTabEmbeded(LPCSTR pInfile);

int    guessRecLen(LPCSTR pInfile, int iLineLen);
int    appendFixFile(char *pFile1, char *pFile2, int iLen);
int    appendTxtFile(char *pFile1, char *pFile2);

bool   isValidYMD(char *pDate, bool bChkDate=true, bool bChkYear=true, bool bChkToday=false);
bool   isValidMDY(char *pDate);
LPSTR  isCharIncluded(LPSTR pBuf, char cChar, int iLen);
LPSTR  isCharIncluded(LPSTR pBuf, int iLen);
LPSTR  isNumIncluded(LPSTR pBuf, int iLen=0);
LPSTR  isTabIncluded(LPSTR pBuf, int iLen=0);

int    dollar2Num(char *pDollar, char *pNumStr);
long   dollar2Num(char *pDollar);
double dollar2Double(char *pValue);

double atofn(char *pStr, int iLen, bool bChkSign=false);
int    atoin(char *pStr, int iLen, bool bRemComma=false);
ULONG  atoln(char *pStr, int iLen, bool bRemComma=false);
long   astol(char *pStr);
void   getCurDate(char *pDate);
int    getCurYear(char *pDate=NULL);
char  *getMonthAbbr(int iMonth);
int    getCurMonth(char *pBuf=NULL);
void   setToday(int iToday);

int    getExtendDate(char *pDate, int lDays);

void   DoValidDate(char *pBuf, int iLen);
int    delFile(char *pFilename);

int    gdate_weekday(long daynr);
int    gdate_monthdays(int month, int year);
long   gdate_dmytoday(int yr, int month, int day);
void   gdate_daytodmy(long days, int *pYr, int *pMonth, int *pDay);
bool   isLeapYear(long year);

long   GregorianToJulian(LPCSTR pGDate, LPSTR pJDate=NULL);
long   JulianToGregorian(LPCSTR pJDate, LPSTR pGDate=NULL);

void   getNextMonth(LPSTR pBuf, int iMonths=1);
void   getPrevMonth(LPSTR pBuf, int iMonths=1);

void   fixDec(LPSTR pStr, int iPos);
void   touch(LPCSTR pFilename);
int    touch(LPCSTR pFile1, LPCSTR pFile2);


// Check for even number of double quotes
int    myGetStrEQ(LPSTR pBuf, int iBufLen, FILE *fd); 
// Check for quote as last byte
int    myGetStrQC(LPSTR pBuf, int iBufLen, FILE *fd); 
// Check for separator as well.  See desc in Utils.cpp
int    myGetStrQC(LPSTR pBuf, char cSeparator, int iBufLen, FILE *fd);
// Check for digit as last char
int    myGetStrDC(LPSTR pBuf, int iBufLen, FILE *fd); 
// Check for special character as first byte and digit as the second byte
// to start a record
int    myGetStrBD(char *pBuf, char bChar, int iBufLen, FILE *fd);
// Check for number of tokens in a string
int    myGetStrTC(char *pBuf, unsigned char cDelimiter, int iCount, int iBufLen, FILE *fd);

int    mkpath(char *pPath);
long   getFileDate(LPCSTR pFilename, LPSTR timeStr=NULL);
__int64 getFileSize(LPCSTR pFilename);

// Type: 1=year, 2=yyyymm, 3=yyyymmdd
int    countDate(LPSTR pFilename, long lDate, long lOffset, long lType);
int    countChar(LPCSTR pBuf, char bChar);

// Read a record at iRecNum and count number of tokens, assuming CSV file
int    countToken(LPCSTR pFilename, int iRecNum, char cDelimiter);

// Count token, ignore delimiter inside double quote
int    countTokens(LPSTR pString, char cDelimiter);

// Count number of lines in a text file
int    countLine(LPCSTR pFilename);

// Reset a character in the file for every line at specific place
// Output the result to *.out
int resetOneChar(LPSTR pInfile, long lOffset, char bRepl);

// Replace unprintable chars in a file by space
int replBadChars(char* pInfile, char *pOutfile, int iLen=0);
int replBadChar(unsigned char* pBuf, int iLen);

HRESULT __stdcall UnicodeToAnsi(wchar_t *pszW, LPSTR pszA, int iLen) ;
HRESULT __stdcall UnicodeToAnsi(LPCSTR pInfile, LPCSTR pOutfile); 

CString myGetAppVersion(LPSTR pBuffer=NULL);
CString toProperCase(CString& sWord);
char    *toProperCase(char *pWord, int iLen=0);

int   CreateFlgFile(char *pCnty, char *pRawTmpl);
int   RemoveTempFiles(char *pCnty, char *pTmpPath, bool bDelZip=false);
int   Check(bool bCorrect, LPCSTR pMsg);
int   RenameToExt(LPSTR pFilename, LPSTR pExt="sav");
int   RebuildCsv(LPSTR pInfile, LPSTR pOutfile, unsigned char cDelim, int iTokens);
int   FixDollarField(LPSTR pInfile, LPSTR pOutfile);

void  InitUtils(LPCSTR pIniFile);
int   GetIniString(LPCSTR pSection, LPCSTR pItem, LPCSTR pDefVal, LPSTR pVal, int iMaxBuf, LPCSTR pIniFile);
int   vmemcpy(LPSTR pDest, LPSTR pSrc, int iMaxLen, int iStrLen=0);
int   SafeDelete(LPCSTR pFilepath);
char  *GetExePath(LPSTR pExePath, LPCSTR pArg0);
char  *_strstrn(char *pString, char *pSearch, int iMaxLen);
char  *timeConv(time_t t);
int   countWord(char *pStr);
int   DayOfWeek(int iYear, int iMonth, int Day);
char  *remCrLf(char *pString);

// Convert zone decimal to signed number
double Zone2Dbl(char *pZoneValue, int iLen, double dDiv=100.0);
int    Zone2Int(char *pZoneValue, int iLen);

//  If file1 is newer, return 1. If file2 is newer, return 2. If they are the same, return 0.
int   chkFileDate(LPCSTR pFile1, LPCSTR pFile2);
int   chkFileDateTime(LPCSTR pFile1, LPCSTR pFile2);
int   ChkBlankRow(LPCSTR pInFile, LPCSTR pOutFile, int iOffset=0);
char  *AddString(char *pOutStr, char *pInStr, char *pDelim, bool bAfter);
void	AppendString(char *pOutStr, char *pInStr, char *pDelim, bool bAfter);
char  *FindLatestFile(char *pSrcDir, char *pFileName, char *pDstDir);
int   LaunchApp(char *pApp, char *pArgv);
int   chkFileStart(LPSTR pInFile, LPSTR pWord, int iStartAt=0);
bool  chkSignValue(char *pValue, int iSize);
char  *strRight(char *pInput, int iCnt, char *pOutput=NULL);

#endif