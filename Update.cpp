#include "stdafx.h"
#include "Logs.h"
#include "Update.h"
#include "Utils.h"

static HANDLE fhUpd;

extern char acIniFile[];
int    m_iNumChk;
bool   m_bDebug;

/*********************************** updOpen ********************************
 *
 * Open file for update
 *
 ****************************************************************************/

HANDLE updOpen(char *pCnty)
{
   char acUpdFile[_MAX_PATH], acUpdTmpl[_MAX_PATH];

   GetIniString("Data", "UpdFile", "", acUpdTmpl, _MAX_PATH, acIniFile);
   sprintf(acUpdFile, acUpdTmpl, pCnty, pCnty, "UPD");

   // Open Update file
   LogMsg("Open Update file %s", acUpdFile);
   fhUpd = CreateFile(acUpdFile, GENERIC_WRITE, 0, NULL, OPEN_ALWAYS,
        FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

   if (fhUpd == INVALID_HANDLE_VALUE)
   {
      LogMsgD("***** Error opening update file %s ", acUpdFile);
   } else
   {
      // Seek to end of file
      if (SetFilePointer(fhUpd, 0, NULL, FILE_END) == -1)
      {
         CloseHandle(fhUpd);
         LogMsgD("***** Error seek to eof %s ", acUpdFile);
         fhUpd = INVALID_HANDLE_VALUE;
      }
   }

   return fhUpd;
}

/********************************** updClose ********************************
 *
 *
 ****************************************************************************/

void updClose()
{
   if (fhUpd > 0)
   {
      CloseHandle(fhUpd);
      fhUpd = 0;
   }
}

/********************************** updUpdate *******************************
 *
 *
 ****************************************************************************/

BOOL updUpdate(char *pOrgRec, char *pUpdRec, int iLen)
{
   DWORD nBytesWritten;
   BOOL  bRet=false;

   if (memcmp(pUpdRec, pOrgRec, iLen))
   {
      bRet = WriteFile(fhUpd, pUpdRec, iLen, &nBytesWritten, NULL);
      if (!bRet)
         LogMsgD("***** Error writing to update file");
   }

   return bRet;
}

/********************************** updWrite ********************************
 *
 *
 ****************************************************************************/

BOOL updWrite(char *pUpdRec, int iLen)
{
   DWORD nBytesWritten;
   BOOL  bRet=false;

   //*(pUpdRec+OFF_AR_CODE3) = 'N';       // Flag new record
   bRet = WriteFile(fhUpd, pUpdRec, iLen, &nBytesWritten, NULL);
   if (!bRet)
      LogMsgD("***** Error writing to update file 0x%.8x", GetLastError());

   return bRet;
}

/********************************** updFileCmp ******************************
 *
 * Compare File1 to File2.  Output File2 records that are different from File1 or
 * not in File1 to pUpdFile if supplied.  An APN is output to pApnList with flag
 * that specifies reason.  If ApnFile specified, create list of new APN only.
 *
 * Return number of output records, negative value if error
 *
 ****************************************************************************/

int updFileCmp(char *pFile1, char *pFile2, char *pFileUpd, char *pApnList, char *pNewApnFile, int iRecordLen, int iApnLen)
{
   char     acBuf1[2048], acBuf2[2048], acTmp[16];
   HANDLE   fhIn1, fhIn2;
   FILE     *fdApnList, *fdNewApn;

   int      iTmp, iCmp, iNewRec=0, iUpdRec=0, iDelRec=0;
   DWORD    nBytesRead;
   BOOL     bRet, bReadRet, bEof1, bEof2;
   long     lRet, lCnt=0;
   APNLIST  myApnList;

   LogMsgD("\nCompare: %s --> %s", pFile1, pFile2);

   if (_access(pFile1, 0))
   {
      LogMsgD("***** Missing input file %s.  Please recheck!", pFile1);
      return -1;
   }

   if (_access(pFile2, 0))
   {
      LogMsgD("***** Missing input file %s.  Please recheck!", pFile2);
      return -1;
   }

   // Open Input file
   LogMsg("Open input file %s", pFile1);
   fhIn1 = CreateFile(pFile1, GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING,
          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

   if (fhIn1 == INVALID_HANDLE_VALUE)
   {
      LogMsg("***** Error opening input file: %s\n", pFile1);
      return -1;
   }

   LogMsg("Open input file %s", pFile2);
   fhIn2 = CreateFile(pFile2, GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING,
          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

   if (fhIn2 == INVALID_HANDLE_VALUE)
   {
      LogMsg("***** Error opening input file: %s\n", pFile2);
      return -1;
   }

   // Open APN list file
   if (!(fdApnList = fopen(pApnList, "a+")))
   {
      LogMsg("***** Error opening APN list file: %s\n", pApnList);
      return -1;
   }

   if (pNewApnFile)
   {
      // Open Update file
      LogMsg("Open new APN file %s", pNewApnFile);
      fdNewApn = fopen(pNewApnFile, "a+");
   } else
      fdNewApn = NULL;

   if (pFileUpd)
   {
      // Open Update file
      LogMsg("Open Update file %s", pFileUpd);
      fhUpd = CreateFile(pFileUpd, GENERIC_WRITE, 0, NULL, OPEN_ALWAYS,
           FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

      if (fhUpd == INVALID_HANDLE_VALUE)
      {
         LogMsgD("***** Error opening update file %s ", pFileUpd);
         return -1;
      } else
      {
         // Seek to end of file
         if (SetFilePointer(fhUpd, 0, NULL, FILE_END) == -1)
         {
            CloseHandle(fhUpd);
            LogMsgD("***** Error seek to eof %s ", pFileUpd);
            return -1;
         }
      }
   } else
      fhUpd = 0;

   memset(&myApnList, ' ', sizeof(APNLIST));
   myApnList.CrLf[0] = '\n';
   myApnList.CrLf[1] = 0;

   bReadRet = ReadFile(fhIn2, acBuf2, iRecordLen, &nBytesRead, NULL);

   // Compare loop
   bEof1 = bEof2 = false;
   while (!bEof1 && !bEof2)
   {
      bReadRet = ReadFile(fhIn1, acBuf1, iRecordLen, &nBytesRead, NULL);

      // Check for EOF
      if (!bReadRet)
      {
         LogMsg("***** Error reading input file %s (%f)", pFile1, GetLastError());
         break;
      }

      // EOF ?
      if (iRecordLen != nBytesRead)
      {
         bEof1 = true;
         break;
      }

ReCompare:
      myApnList.bUpdDel = ' ';
      iCmp = memcmp(acBuf1, acBuf2, iApnLen);
      if (!iCmp)
      {
         // If same APN, compare whole record
         if (m_bDebug)
         {
            for (iTmp = 0; iTmp < iRecordLen; iTmp++)
            {
               if (acBuf1[iTmp] != acBuf2[iTmp])
                  break;
            }

            if (iTmp == iRecordLen)
               iTmp = 0;
         } else
            iTmp = memcmp(acBuf1, acBuf2, iRecordLen);

         if (iTmp)
         {
            if (fhUpd)
            {
               if (m_bDebug)
               {
                  lRet = sprintf(acTmp, "[%d]   ", iTmp);
                  memcpy(&acBuf2[31], acTmp, lRet);
               }
               bRet = updWrite(acBuf2, iRecordLen);
            }
            iUpdRec++;

            memcpy(myApnList.Apn, acBuf2, iApnLen);
            myApnList.bUpdDel = UPDATE_ROLL_REC;
         }

         // Read new record
         bReadRet = ReadFile(fhIn2, acBuf2, iRecordLen, &nBytesRead, NULL);
      } else if (iCmp > 0)
      {  // new record in File2
         if (fhUpd)
            bRet = updWrite(acBuf2, iRecordLen);

         memcpy(myApnList.Apn, acBuf2, iApnLen);
         myApnList.bUpdDel = INSERT_ROLL_REC;

         if (fdNewApn)
         {

            if (acBuf2[iApnLen-1] == ' ')
            {
               acBuf2[iApnLen-1] = '\n';
               acBuf2[iApnLen] = '\0';
            } else
            {
               acBuf2[iApnLen] = '\n';
               acBuf2[iApnLen+1] = '\0';
            }
            fputs(acBuf2, fdNewApn);
         }

         // Get next input record from file2
         bReadRet = ReadFile(fhIn2, acBuf2, iRecordLen, &nBytesRead, NULL);
         iNewRec++;
      } else
      {  // retired record - skip 
         bReadRet = 1;
         iDelRec++;

         memcpy(myApnList.Apn, acBuf1, iApnLen);
         myApnList.bUpdDel = DELETE_ROLL_REC;
      }

      // Output APN rec
      if (myApnList.bUpdDel > ' ')
         fputs((char *)&myApnList, fdApnList);

      if (!(++lCnt % 1000))
         printf("\r%u\t%u\t%u\t%u", lCnt, iUpdRec, iDelRec, iNewRec);

      if (m_iNumChk > 0 && lCnt > m_iNumChk)
      {
         lRet = -999;
         bEof1 = true;
         bEof2 = true;
         break;
      }

      if (!bReadRet)
      {
         LogMsg("***** Error reading input file %s (%f)", pFile2, GetLastError());
         lRet = -99;
         break;
      }

      if (iRecordLen != nBytesRead)
      {
         bEof2 = true;
         break;
      }

      // Old APN > new APN, let recompare
      if (iCmp > 0)
         goto ReCompare;
   }

   // If file1 is not EOF, mark remaining APN to be deleted
   while (!bEof1)
   {
      // Output APN rec
      if (myApnList.bUpdDel > ' ' && myApnList.bUpdDel != UPDATE_ROLL_REC)
      {
         memcpy(myApnList.Apn, acBuf1, iApnLen);
         myApnList.bUpdDel = DELETE_ROLL_REC;
         fputs((char *)&myApnList, fdApnList);
         iDelRec++;
      } else
         myApnList.bUpdDel = ' ';

      // Read next rec
      bReadRet = ReadFile(fhIn1, acBuf1, iRecordLen, &nBytesRead, NULL);
      if (!(++lCnt % 1000))
         printf("\r%u\t%u\t%u\t%u", lCnt, iUpdRec, iDelRec, iNewRec);

      if (!bReadRet)
      {
         LogMsg("***** Error reading input file %s (%f)", pFile1, GetLastError());
         lRet = -99;
         break;
      }

      if (iRecordLen != nBytesRead)
         bEof1 = true;
   }

   while (!bEof2)
   {
      // Output APN rec
      memcpy(myApnList.Apn, acBuf2, iApnLen);
      myApnList.bUpdDel = INSERT_ROLL_REC;
      fputs((char *)&myApnList, fdApnList);

      // Output Upd rec
      if (fhUpd)
         bRet = updWrite(acBuf2, iRecordLen);

      if (fdNewApn)
      {
         if (acBuf2[iApnLen-1] == ' ')
         {
            acBuf2[iApnLen-1] = '\n';
            acBuf2[iApnLen] = '\0';
         } else
         {
            acBuf2[iApnLen] = '\n';
            acBuf2[iApnLen+1] = '\0';
         }
         fputs(acBuf2, fdNewApn);
      }

      bReadRet = ReadFile(fhIn2, acBuf2, iRecordLen, &nBytesRead, NULL);
      iNewRec++;

      if (!bReadRet)
      {
         LogMsg("***** Error reading input file %s (%f)", pFile2, GetLastError());
         lRet = -99;
         break;
      }

      if (!(++lCnt % 1000))
         printf("\r%u\t%u\t%u\t%u", lCnt, iUpdRec, iDelRec, iNewRec);

      if (iRecordLen != nBytesRead)
         bEof2 = true;
   }

   // Close files
   if (fhIn1)
      CloseHandle(fhIn1);
   if (fhIn2)
      CloseHandle(fhIn2);

   if (fdApnList)
      fclose(fdApnList);
   if (fdNewApn)
      fclose(fdNewApn);

   if (fhUpd)
      updClose();

   printf("\r%u\t%u\t%u\t%u\n", lCnt, iUpdRec, iDelRec, iNewRec);
   LogMsgD("Total new records:          %u", iNewRec);
   LogMsgD("Total changed records:      %u", iUpdRec);
   LogMsgD("Total deleted records:      %u", iDelRec);

   return iNewRec+iUpdRec+iDelRec;
}

int updFileCmpi(char *pFile1, char *pFile2, char *pFileUpd, char *pApnList, int iRecordLen, int iApnLen)
{
   char     acBuf1[2048], acBuf2[2048], acTmp[16];
   HANDLE   fhIn1, fhIn2;
   FILE     *fdApnList;

   int      iTmp, iCmp, iNewRec=0, iUpdRec=0, iDelRec=0;
   DWORD    nBytesRead;
   BOOL     bRet, bReadRet, bEof1, bEof2;
   long     lRet, lCnt=0;
   APNLIST  myApnList;

   LogMsgD("\nCompare: %s --> %s", pFile1, pFile2);

   if (_access(pFile1, 0))
   {
      LogMsgD("***** Missing input file %s.  Please recheck!", pFile1);
      return -1;
   }

   if (_access(pFile2, 0))
   {
      LogMsgD("***** Missing input file %s.  Please recheck!", pFile2);
      return -1;
   }

   // Open Input file
   LogMsg("Open input file %s", pFile1);
   fhIn1 = CreateFile(pFile1, GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING,
          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

   if (fhIn1 == INVALID_HANDLE_VALUE)
   {
      LogMsg("***** Error opening input file: %s\n", pFile1);
      return -1;
   }

   LogMsg("Open input file %s", pFile2);
   fhIn2 = CreateFile(pFile2, GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING,
          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

   if (fhIn2 == INVALID_HANDLE_VALUE)
   {
      LogMsg("***** Error opening input file: %s\n", pFile2);
      return -1;
   }

   // Open APN list file
   if (!(fdApnList = fopen(pApnList, "a+")))
   {
      LogMsg("***** Error opening APN list file: %s\n", pApnList);
      return -1;
   }

   if (pFileUpd)
   {
      // Open Update file
      LogMsg("Open Update file %s", pFileUpd);
      fhUpd = CreateFile(pFileUpd, GENERIC_WRITE, 0, NULL, OPEN_ALWAYS,
           FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

      if (fhUpd == INVALID_HANDLE_VALUE)
      {
         LogMsgD("***** Error opening update file %s ", pFileUpd);
         return -1;
      } else
      {
         // Seek to end of file
         if (SetFilePointer(fhUpd, 0, NULL, FILE_END) == -1)
         {
            CloseHandle(fhUpd);
            LogMsgD("***** Error seek to eof %s ", pFileUpd);
            return -1;
         }
      }
   } else
      fhUpd = 0;

   memset(&myApnList, ' ', sizeof(APNLIST));
   myApnList.CrLf[0] = '\n';
   myApnList.CrLf[1] = 0;

   bReadRet = ReadFile(fhIn2, acBuf2, iRecordLen, &nBytesRead, NULL);

   // Merge loop
   bEof1 = bEof2 = false;
   while (!bEof1 && !bEof2)
   {
      bReadRet = ReadFile(fhIn1, acBuf1, iRecordLen, &nBytesRead, NULL);

      // Check for EOF
      if (!bReadRet)
      {
         LogMsg("***** Error reading input file %s (%f)", pFile1, GetLastError());
         break;
      }

      // EOF ?
      if (iRecordLen != nBytesRead)
      {
         bEof1 = true;
         break;
      }

ReCompare:
      myApnList.bUpdDel = ' ';
      iCmp = _memicmp(acBuf1, acBuf2, iApnLen);
      if (!iCmp)
      {
         // If same APN, compare whole record
         if (m_bDebug)
         {
            for (iTmp = 0; iTmp < iRecordLen; iTmp++)
            {
               if (acBuf1[iTmp] != acBuf2[iTmp])
                  break;
            }

            if (iTmp == iRecordLen)
               iTmp = 0;
         } else
            iTmp = _memicmp(acBuf1, acBuf2, iRecordLen);

         if (iTmp)
         {
            if (fhUpd)
            {
               if (m_bDebug)
               {
                  lRet = sprintf(acTmp, "[%d]   ", iTmp);
                  memcpy(&acBuf2[31], acTmp, lRet);
               }
               bRet = updWrite(acBuf2, iRecordLen);
            }
            iUpdRec++;

            memcpy(myApnList.Apn, acBuf2, iApnLen);
            myApnList.bUpdDel = UPDATE_ROLL_REC;
         }

         // Read new record
         bReadRet = ReadFile(fhIn2, acBuf2, iRecordLen, &nBytesRead, NULL);
      } else if (iCmp > 0)
      {  // new record in File2
         if (fhUpd)
            bRet = updWrite(acBuf2, iRecordLen);

         memcpy(myApnList.Apn, acBuf2, iApnLen);
         myApnList.bUpdDel = INSERT_ROLL_REC;

         // Get next input record from file2
         bReadRet = ReadFile(fhIn2, acBuf2, iRecordLen, &nBytesRead, NULL);
         iNewRec++;
      } else
      {  // retired record - skip 
         bReadRet = 1;
         iDelRec++;

         memcpy(myApnList.Apn, acBuf1, iApnLen);
         myApnList.bUpdDel = DELETE_ROLL_REC;
      }

      // Output APN rec
      if (myApnList.bUpdDel > ' ')
         fputs((char *)&myApnList, fdApnList);

      if (!(++lCnt % 1000))
         printf("\r%u\t%u\t%u\t%u", lCnt, iUpdRec, iDelRec, iNewRec);

      if (m_iNumChk > 0 && lCnt > m_iNumChk)
      {
         lRet = -999;
         bEof1 = true;
         bEof2 = true;
         break;
      }

      if (!bReadRet)
      {
         LogMsg("***** Error reading input file %s (%f)", pFile2, GetLastError());
         lRet = -99;
         break;
      }

      if (iRecordLen != nBytesRead)
      {
         bEof2 = true;
         break;
      }

      // Old APN > new APN, let recompare
      if (iCmp > 0)
         goto ReCompare;
   }

   // If file1 is not EOF, mark remaining APN to be deleted
   while (!bEof1)
   {
      // Output APN rec
      if (myApnList.bUpdDel > ' ' && myApnList.bUpdDel != UPDATE_ROLL_REC)
      {
         memcpy(myApnList.Apn, acBuf1, iApnLen);
         myApnList.bUpdDel = DELETE_ROLL_REC;
         fputs((char *)&myApnList, fdApnList);
         iDelRec++;
      } else
         myApnList.bUpdDel = ' ';

      // Read next rec
      bReadRet = ReadFile(fhIn1, acBuf1, iRecordLen, &nBytesRead, NULL);
      if (!(++lCnt % 1000))
         printf("\r%u\t%u\t%u\t%u", lCnt, iUpdRec, iDelRec, iNewRec);

      if (!bReadRet)
      {
         LogMsg("***** Error reading input file %s (%f)", pFile1, GetLastError());
         lRet = -99;
         break;
      }

      if (iRecordLen != nBytesRead)
         bEof1 = true;
   }

   while (!bEof2)
   {
      // Output APN rec
      memcpy(myApnList.Apn, acBuf2, iApnLen);
      myApnList.bUpdDel = INSERT_ROLL_REC;
      fputs((char *)&myApnList, fdApnList);

      // Output Upd rec
      if (fhUpd)
         bRet = updWrite(acBuf2, iRecordLen);
      bReadRet = ReadFile(fhIn2, acBuf2, iRecordLen, &nBytesRead, NULL);
      iNewRec++;

      if (!bReadRet)
      {
         LogMsg("***** Error reading input file %s (%f)", pFile2, GetLastError());
         lRet = -99;
         break;
      }

      if (!(++lCnt % 1000))
         printf("\r%u\t%u\t%u\t%u", lCnt, iUpdRec, iDelRec, iNewRec);

      if (iRecordLen != nBytesRead)
         bEof2 = true;
   }

   // Close files
   if (fhIn1)
      CloseHandle(fhIn1);
   if (fhIn2)
      CloseHandle(fhIn2);

   if (fdApnList)
      fclose(fdApnList);

   if (fhUpd)
      updClose();

   printf("\r%u\t%u\t%u\t%u\n", lCnt, iUpdRec, iDelRec, iNewRec);
   LogMsgD("Total new records:          %u", iNewRec);
   LogMsgD("Total changed records:      %u", iUpdRec);
   LogMsgD("Total deleted records:      %u", iDelRec);

   return iNewRec+iUpdRec+iDelRec;
}

int updFileCmp2(char *pFile1, char *pFile2, char *pFileUpd, int iRecordLen, int iApnLen)
{
   char     acBuf1[2048], acBuf2[2048];
   HANDLE   fhIn1, fhIn2;

   int      iTmp, iNewRec=0, iUpdRec=0, iDelRec=0;
   DWORD    nBytesRead;
   BOOL     bRet, bReadRet, bEof1, bEof2;
   long     lRet, lCnt=0;

   LogMsgD("\nCompare: %s --> %s", pFile1, pFile2);

   if (_access(pFile1, 0))
   {
      LogMsgD("***** Missing input file %s.  Please recheck!", pFile1);
      return -1;
   }

   if (_access(pFile2, 0))
   {
      LogMsgD("***** Missing input file %s.  Please recheck!", pFile2);
      return -1;
   }


   // Open Input file
   LogMsg("Open input file %s", pFile1);
   fhIn1 = CreateFile(pFile1, GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING,
          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

   if (fhIn1 == INVALID_HANDLE_VALUE)
   {
      LogMsg("***** Error opening input file: %s\n", pFile1);
      return -1;
   }

   LogMsg("Open input file %s", pFile2);
   fhIn2 = CreateFile(pFile2, GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING,
          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

   if (fhIn2 == INVALID_HANDLE_VALUE)
   {
      LogMsg("***** Error opening input file: %s\n", pFile2);
      return -1;
   }

   // Open Update file
   LogMsg("Open Update file %s", pFileUpd);
   fhUpd = CreateFile(pFileUpd, GENERIC_WRITE, 0, NULL, OPEN_ALWAYS,
        FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

   if (fhUpd == INVALID_HANDLE_VALUE)
   {
      LogMsgD("***** Error opening update file %s ", pFileUpd);
      return -1;
   }

   bReadRet = ReadFile(fhIn2, acBuf2, iRecordLen, &nBytesRead, NULL);

   // Merge loop
   bEof1 = bEof2 = false;
   while (!bEof1 && !bEof2)
   {
      bReadRet = ReadFile(fhIn1, acBuf1, iRecordLen, &nBytesRead, NULL);

      // Check for EOF
      if (!bReadRet)
      {
         LogMsg("***** Error reading input file %s (%f)", pFile1, GetLastError());
         break;
      }

      // EOF ?
      if (iRecordLen != nBytesRead)
      {
         bEof1 = true;
         break;
      }

ReCompare:
      iTmp = memcmp(acBuf1, acBuf2, iApnLen);
      if (!iTmp)
      {
         // If same APN, compare whole record
         if (memcmp(acBuf1, acBuf2, iRecordLen))
         {
            bRet = updWrite(acBuf2, iRecordLen);
            iUpdRec++;
         }

         // Read new record
         bReadRet = ReadFile(fhIn2, acBuf2, iRecordLen, &nBytesRead, NULL);
      } else if (iTmp > 0)
      {  // new record in File2
         bReadRet = ReadFile(fhIn2, acBuf2, iRecordLen, &nBytesRead, NULL);
         iNewRec++;
      } else
      {  // retired record - skip 
         bReadRet = 1;
         iDelRec++;
      }

      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);

      if (m_iNumChk > 0 && lCnt > m_iNumChk)
      {
         lRet = -999;
         break;
      }

      if (!bReadRet)
      {
         LogMsg("***** Error reading input file %s (%f)", pFile2, GetLastError());
         lRet = -99;
         break;
      }

      if (iRecordLen != nBytesRead)
      {
         bEof2 = true;
         break;
      }

      if (iTmp > 0)
         goto ReCompare;
   }

   // Close files
   if (fhIn1)
      CloseHandle(fhIn1);
   if (fhIn2)
      CloseHandle(fhIn2);

   updClose();

   printf("\n");
   LogMsgD("Total output records:       %u", lCnt);
   LogMsgD("Total new records:          %u", iNewRec);
   LogMsgD("Total changed records:      %u", iUpdRec);
   LogMsgD("Total deleted records:      %u", iDelRec);

   return iNewRec+iUpdRec+iDelRec;
}

/******************************** updCmpS01R01 ******************************
 *
 * Return number of output records, negative value if error
 *
 ****************************************************************************/

int updCmpS01R01(char *pCnty, char *pRawTmpl, int iRecordLen, int iApnLen)
{
   char  acS01[_MAX_PATH], acR01[_MAX_PATH], acUpd[_MAX_PATH], acApnList[_MAX_PATH], acUpdPath[_MAX_PATH];
   int   iRet;
	bool  bCase=true;

   GetPrivateProfileString("System", "CaseSensitive", "Y", acUpdPath, _MAX_PATH, acIniFile);
	if (acUpdPath[0] == 'N')
		bCase = false;

   GetIniString("Data", "UpdFile", pRawTmpl, acUpdPath, _MAX_PATH, acIniFile);
   sprintf(acApnList, acUpdPath, pCnty, pCnty, "CHK");
   sprintf(acUpd, acUpdPath, pCnty, pCnty, "UPD");
   sprintf(acS01, pRawTmpl, pCnty, pCnty, "S01");
   sprintf(acR01, pRawTmpl, pCnty, pCnty, "R01");
	if (bCase)
		iRet = updFileCmp(acS01, acR01, acUpd, acApnList, NULL, iRecordLen, iApnLen);
	else
		iRet = updFileCmpi(acS01, acR01, acUpd, acApnList, iRecordLen, iApnLen);

   if (!memcmp(pCnty, "LAX", 3))
   {
      sprintf(acS01, pRawTmpl, pCnty, pCnty, "S02");
      sprintf(acR01, pRawTmpl, pCnty, pCnty, "R02");
		if (bCase)
			iRet += updFileCmp(acS01, acR01, acUpd, acApnList, NULL, iRecordLen, iApnLen);
		else
			iRet += updFileCmpi(acS01, acR01, acUpd, acApnList, iRecordLen, iApnLen);
      sprintf(acS01, pRawTmpl, pCnty, pCnty, "S03");
      sprintf(acR01, pRawTmpl, pCnty, pCnty, "R03");
		if (bCase)
			iRet += updFileCmp(acS01, acR01, acUpd, acApnList, NULL, iRecordLen, iApnLen);
		else
			iRet += updFileCmpi(acS01, acR01, acUpd, acApnList, iRecordLen, iApnLen);
   }

   return iRet;
}

int updCmpX01G01(char *pCnty, char *pRawTmpl, int iRecordLen, int iApnLen)
{
   char  acS01[_MAX_PATH], acR01[_MAX_PATH], acUpd[_MAX_PATH];
   int   iRet;

   sprintf(acUpd, pRawTmpl, pCnty, "UPD");
   sprintf(acS01, pRawTmpl, pCnty, "X01");
   sprintf(acR01, pRawTmpl, pCnty, "G01");
   iRet = updFileCmp2(acS01, acR01, acUpd, iRecordLen, iApnLen);

   if (!memcmp(pCnty, "LAX", 3))
   {
      sprintf(acS01, pRawTmpl, pCnty, "X02");
      sprintf(acR01, pRawTmpl, pCnty, "G02");
      iRet += updFileCmp2(acS01, acR01, acUpd, iRecordLen, iApnLen);
      sprintf(acS01, pRawTmpl, pCnty, "X03");
      sprintf(acR01, pRawTmpl, pCnty, "G03");
      iRet += updFileCmp2(acS01, acR01, acUpd, iRecordLen, iApnLen);
   }

   return iRet;
}

/*********************************** chkS01R01 ******************************
 *
 * Return number of output records, negative value if error.
 * Compare S01 & R01 files and output S???.CHK file only.
 *
 ****************************************************************************/

int chkS01R01(char *pCnty, char *pRawTmpl, int iRecordLen, int iApnLen, bool bCreateUpdate, bool bCreateApnFile)
{
   char  acS01[_MAX_PATH], acR01[_MAX_PATH], acApnList[_MAX_PATH], acUpdList[_MAX_PATH], acUpdPath[_MAX_PATH], acNewApnFile[_MAX_PATH];
   int   iRet;

   GetIniString("Data", "UpdFile", pRawTmpl, acUpdPath, _MAX_PATH, acIniFile);
   sprintf(acApnList, acUpdPath, pCnty, pCnty, "CHK");
   sprintf(acS01, pRawTmpl, pCnty, pCnty, "S01");
   sprintf(acR01, pRawTmpl, pCnty, pCnty, "R01");

   if (bCreateApnFile)
   {
      sprintf(acNewApnFile, acUpdPath, pCnty, pCnty, "NewApn");
      iRet = updFileCmp(acS01, acR01, NULL, acApnList, acNewApnFile, iRecordLen, iApnLen);
   } else if (bCreateUpdate)
   {
      sprintf(acUpdList, acUpdPath, pCnty, pCnty, "UPD");
      iRet = updFileCmp(acS01, acR01, acUpdList, acApnList, NULL, iRecordLen, iApnLen);
   } else
      iRet = updFileCmp(acS01, acR01, NULL,      acApnList, NULL, iRecordLen, iApnLen);

   if (!memcmp(pCnty, "LAX", 3))
   {
      sprintf(acS01, pRawTmpl, pCnty, pCnty, "S02");
      sprintf(acR01, pRawTmpl, pCnty, pCnty, "R02");
      if (bCreateApnFile)
         iRet = updFileCmp(acS01, acR01, NULL, acApnList, acNewApnFile, iRecordLen, iApnLen);
      else if (bCreateUpdate)
         iRet += updFileCmp(acS01, acR01, acUpdList, acApnList, NULL, iRecordLen, iApnLen);
      else
         iRet += updFileCmp(acS01, acR01, NULL, acApnList, NULL, iRecordLen, iApnLen);
      sprintf(acS01, pRawTmpl, pCnty, pCnty, "S03");
      sprintf(acR01, pRawTmpl, pCnty, pCnty, "R03");
      if (bCreateApnFile)
         iRet = updFileCmp(acS01, acR01, NULL, acApnList, acNewApnFile, iRecordLen, iApnLen);
      else if (bCreateUpdate)
         iRet += updFileCmp(acS01, acR01, acUpdList, acApnList, NULL, iRecordLen, iApnLen);
      else
         iRet += updFileCmp(acS01, acR01, NULL, acApnList, NULL, iRecordLen, iApnLen);
   }

   // Remove CHK file if no change
   if (!iRet && !_access(acApnList, 0))
      DeleteFile(acApnList);

   return iRet;
}

