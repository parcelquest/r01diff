// R01Diff.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include "Prodlib.h"
#include "CountyInfo.h"
#include "Utils.h"
#include "Logs.h"
#include "Getopt.h"
#include "R01Diff.h"
#include "Update.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// The one and only application object

CWinApp theApp;

using namespace std;

extern bool m_bDebug;
extern int  m_iNumChk;

char  acIniFile[_MAX_PATH], acRawTmpl[_MAX_PATH], acFile1[_MAX_PATH], acFile2[_MAX_PATH];
int   iRecLen, iG01Len;
bool  bUseG01;

COUNTY_INFO myCounty;

/******************************** LoadCountyInfo ******************************
 *
 * Initialize global variables
 *
 *****************************************************************************/

int LoadCountyInfo(char *pCntyCode, char *pCntyTbl)
{
   char     acTmp[_MAX_PATH], *pTmp, *pFlds[MAX_CNTY_FLDS];
   int      iRet=0, iTmp;
   FILE     *fd;

   fd = fopen(pCntyTbl, "r");
   if (fd)
   {
      // Skip blank line
      pTmp = fgets(acTmp, _MAX_PATH, fd);

      while (!feof(fd))
      {
         pTmp = fgets(acTmp, _MAX_PATH, fd);
         if (pTmp)
         {
            if (!memcmp(acTmp, pCntyCode, 3))
            {
               iTmp = ParseString(myTrim(acTmp), ',', MAX_CNTY_FLDS, pFlds);
               if (iTmp > 0)
               {
                  strcpy(myCounty.acCntyCode, pCntyCode);
                  strcpy(myCounty.acStdApnFmt, pFlds[FLD_APN_FMT]);
                  strcpy(myCounty.acSpcApnFmt[0], pFlds[FLD_SPC_FMT]);
                  strcpy(myCounty.acCase[0], pFlds[FLD_SPC_BOOK]);
                  strcpy(myCounty.acCntyID, pFlds[FLD_CNTY_ID]);
                  strcpy(myCounty.acCntyName, pFlds[FLD_CNTY_NAME]);
                  strcpy(myCounty.acYearAssd, pFlds[FLD_YR_ASSD]);

                  iRet = atoi(myCounty.acCntyID);
                  myCounty.iCntyID = iRet;
                  sprintf(myCounty.acFipsCode, "06%.3d", iRet*2-1);
                  myCounty.iApnLen = atoi(pFlds[FLD_APN_LEN]);
                  myCounty.iBookLen = atoi(pFlds[FLD_BOOK_LEN]);
                  myCounty.iPageLen = atoi(pFlds[FLD_PAGE_LEN]);
                  myCounty.iCmpLen = atoi(pFlds[FLD_CMP_LEN]);
                  iRet = 1;
               } else
               {
                  LogMsg("***** Bad county table file: %s", pCntyTbl);
               }

               break;
            }
         } else
            break;
      }

      fclose(fd);

      if (1 != iRet)
         LogMsg("***** County not found.  Please verify %s", pCntyTbl);
   } else
      LogMsg("***** Error opening county table %s", pCntyTbl);

   return iRet;
}

/********************************** initCounty *******************************
 *
 * Initialize global variables
 *
 *****************************************************************************/

int R01DiffInit(char *pCnty)
{
   char  acTmp[_MAX_PATH], acLogFile[_MAX_PATH];
   int   iRet;

   GetPrivateProfileString("System", "LogPath", "", acTmp, _MAX_PATH, acIniFile);

   // open log file
   sprintf(acLogFile, "%s\\Diff_%.3s.log", acTmp, pCnty);
   open_log(acLogFile, "w");

   // Get debug flag
   GetPrivateProfileString("System", "Debug", "N", acTmp, _MAX_PATH, acIniFile);
   if (acTmp[0] == 'Y')
      m_bDebug = true;
   else
      m_bDebug = false;

   // Get raw file name
   GetPrivateProfileString("Data", "RawFile", "", acRawTmpl, _MAX_PATH, acIniFile);
   iG01Len = GetPrivateProfileInt("Data", "G01Size", 1934, acIniFile);
   if (!iRecLen)
      iRecLen = GetPrivateProfileInt("Data", "RecSize", 1900, acIniFile);

   // Get county info
   GetPrivateProfileString("System", "CountyTbl", ".\\CountyInfo.csv", acTmp, _MAX_PATH, acIniFile);
   printf("Loading county table: %s\n", acTmp);
   iRet = LoadCountyInfo(pCnty, acTmp);

   return iRet;
}

/*****************************************************************************/

int cmpRawFile(char *pCnty, char *pFile1, char *pFile2, int iRecordLen, int iApnLen)
{
   char  acUpd[_MAX_PATH], acApnList[_MAX_PATH], acUpdPath[_MAX_PATH];
   int   iRet;

   GetPrivateProfileString("Data", "UpdFile", acRawTmpl, acUpdPath, _MAX_PATH, acIniFile);
   sprintf(acApnList, acUpdPath, pCnty, pCnty, "CHK");
   sprintf(acUpd, acUpdPath, pCnty, pCnty, "UPD");
   iRet = updFileCmp(pFile1, pFile2, acUpd, acApnList, NULL, iRecordLen, iApnLen);

   return iRet;
}

/************************************** Usage ********************************
 *
 *
 *****************************************************************************/

void Usage()
{
   printf("\nUsage: R01DIFF -C<County Code> [-G] [-S<file1> -D<file2>] [-Xn] (i.e. -CAMA)\n\n");
   printf("\t-C  : County to load\n");
   printf("\t-G  : Compare G01 vs X01 file in CO_TEMP.\n");
   printf("\t-S  : Source file to compare.\n");
   printf("\t-D  : Destination file to compare.\n");
   printf("\t-Xn : Number of records to compare (default all)\n");
   exit(1);
}

/********************************** ParseCmd() *******************************
 *
 *
 *****************************************************************************/

void ParseCmd(int argc, char* argv[])
{
   char  chOpt;               // gotten option character
   char *pszParam;

   if (argv[1][0] != '-')
   {
      strcpy(myCounty.acCntyCode, argv[1]);
      return;
   }

   bUseG01 = false;
   while (1)
   {
      chOpt = GetOption(argc, argv, "C:X:S:D:GLn?", &pszParam);
      if (chOpt > 1)
      {
         // chOpt is valid argument
         switch (chOpt)
         {
            case 'C':                           // county code
               if (pszParam != NULL)
                  strcpy(myCounty.acCntyCode, pszParam);
               else
               {
                  printf("Missing county code\n");
                  Usage();
               }
               break;

            case 'G':                           // Use G01
               bUseG01 = true;
               break;

            case 'L':                           // Compare record length
               if (pszParam && *pszParam > '0')
                  iRecLen = atol(pszParam);
               else
                  Usage();
               break;

            case 'X':                           // Number of records to compare
               if (pszParam && *pszParam > '0')
                  m_iNumChk = atol(pszParam);
               else
                  Usage();
               break;

            case 'S':                           // Compare source
               if (pszParam != NULL)
                  strcpy(acFile1, pszParam);
               else
               {
                  printf("Missing source file.\n");
                  Usage();
               }
               break;

            case 'D':                           // Compare dest
               if (pszParam != NULL)
                  strcpy(acFile2, pszParam);
               else
               {
                  printf("Missing second file.\n");
                  Usage();
               }
               break;

            case '?':   // usage info
            default:
               Usage();
               break;
         }
      }
      if (chOpt == 0)
      {
         // end of argument list
         break;
      }
      if ((chOpt == 1) || (chOpt == -1))
      {
         // standalone param or error
         printf("***** Argument [%s] not recognized\n", pszParam);
         Usage();
      }
   }
}

/*****************************************************************************/

int _tmain(int argc, char* argv[], char* envp[])
{
   char  *pTmp, sVer[128];
	int   iRet;

	// initialize MFC and print and error on failure
	if (!AfxWinInit(::GetModuleHandle(NULL), NULL, ::GetCommandLine(), 0))
	{
		printf("Fatal Error: MFC initialization failed");
		return 1;
	}
   LoadString(theApp.m_hInstance, IDS_VERSION, sVer, 128);
   printf("%s\n", sVer);

   if (argc < 2)
   {
      Usage();
   }

   pTmp = _getcwd((char *)&acIniFile[0], _MAX_PATH);
   strcat(acIniFile, "\\R01Diff.ini");

   // If not found INI file in working folder, check default location
   if (_access(acIniFile, 0))
      strcpy(acIniFile, "C:\\Tools\\R01Diff.ini");

   ParseCmd(argc, (char **)argv);

   iRet = R01DiffInit(myCounty.acCntyCode);
   if (iRet == 1)
   {
      if (bUseG01)
      {
         iRecLen = iG01Len;
         GetPrivateProfileString("Data", "CO_TEMP", "F:\\CO_TEMP\\S%s.%s", acRawTmpl, _MAX_PATH, acIniFile);
         iRet = updCmpX01G01(myCounty.acCntyCode, acRawTmpl, iRecLen, myCounty.iApnLen);
      } else if (acFile1[0] > ' ' && acFile2[0] > ' ')
      {
         iRet = cmpRawFile(myCounty.acCntyCode, acFile1, acFile2, iRecLen, myCounty.iApnLen);
      } else
         iRet = updCmpS01R01(myCounty.acCntyCode, acRawTmpl, iRecLen, myCounty.iApnLen);
   } else
      printf("***** Init failed\n");

   close_log();

	return iRet;
}


