/*****************************************************************************
 *
 * 06/01/2017 Add getMonthAbbr() and getCurmonth() to return month abbr.
 * 07/06/2017 Modify blankRem() to detect null buffer.
 * 07/26/2017 Bug fix dateConversion() on MMDDYY2
 * 08/04/2017 Add FindLatestFile(). This is specific for MPA where tax
 *            subfolder is in the format of "MMM YYYY" i.e. "Jul 2017"
 * 02/21/2018 Add LaunchApp() & executeCommandLine()
 * 07/20/2018 Increase buffer size for myGetStrDC()
 * 10/14/2018 Modify isValidYMD() & dateConversion() to add option to check current year (default true)
 * 12/06/2018 Add code to appendFixFile()
 * 01/03/2019 Increase buffer size in myGetStrTC() from 2K to 8K, RebuildCsv() from 4K to 64K.
 * 05/07/2019 Add chkFileStart()
 * 05/08/2019 Modify chkFileDate() to return -2 if file2 is not present.
 * 06/05/2019 Add chkSignValue() to convert mainframe number to normal.
 * 06/14/2019 Add replBadChar() and modify replBadChars() to replace back slash with space
 * 07/01/2019 Add remDupWords() to remove duplicate words in a string.
 * 07/06/2019 Modify myGetStrTC() to handle delimiter '~' the same as '|'
 * 07/24/2019 Add replBadChar() to remove unprintable char and '\'
 * 08/08/2019 Add remJSChar() to remove Json sensitive char: backslash & double quote.
 *            Fix replBadChar() bug.
 * 08/21/2019 Modify myGetStrTC() to increase sTmp buffer to 65536 for FRE's GrGr.
 * 09/18/2019 Add strRight()
 * 11/18/2019 Add cleanOwner()
 * 03/05/2020 Add countLine()
 * 04/28/2020 Fix bug in chkSignValue()
 * 04/29/2020 Add FixDollarField() to remove extra comma from CSV file.
 * 06/15/2020 Add setToday() and modify isValidYMD() to check for past date.
 * 08/10/2020 Modify countTokens() to bypass myTrim() when delimiter is <tab> character.
 * 10/23/2020 Add remDupStr()
 * 05/12/2021 Modify quoteRem() to return number of quotes removed.
 * 06/03/2021 Modify CreateFlgFile() to create only if the flag file is not there.
 *            Modify myGetStrTC() to insert space when combine two lines.
 * 08/04/2021 Add atoln() to convert to unsigned long.
 *
 *****************************************************************************/

#include "stdafx.h"
#include "prodlib.h"
#include "Utils.h"
#include "Logs.h"
#include "windows.h"

#pragma comment(lib,"Version.lib")

char asMonth[12][4]=
{
   "JAN", "FEB", "MAR", "APR", "MAY", "JUN", "JUL", "AUG", "SEP", "OCT", "NOV", "DEC"
};

CString Days[8]={"","Sunday","Monday","Tuesday","Wednesday","Thursday","Friday","Saturday"}; 

int   monthdays[] = {31,28,31,30,31,30,31,31,30,31,30,31};
int   m_iToday;
char  m_sMachine[64], m_sDataSrc[128];

/********************************* setToday() *******************************
 *
 * Set today date YYYYMMDD
 *
 ****************************************************************************/

void setToday(int iToday)
{
   char  sTmp[64];

   if (iToday > 0)
      m_iToday = iToday;
   else
   {
      getCurDate(sTmp);
      m_iToday = atol(sTmp);
   }
}

/********************************* timeConv() *******************************
 *
 * Convert time_t to string in format: "YYYYMMDD HH:MM:SS"
 *
 ****************************************************************************/

char *timeConv(time_t t)
{
   static char timeStr[80];
   struct tm *pTime  = NULL;
   
   pTime = localtime(&t);
   strftime( timeStr ,80 ,"%Y%m%d %H:%M:%S" ,pTime);
   return timeStr;
}

/********************************** iTrim() *********************************
 *
 * Remove trailing spaces and tabs and other non-printables
 * Return length of string
 *
 ****************************************************************************/

int iTrim(char *pString, int iLen)
{
   int i;

	if (iLen > 0)
	{
		*(pString + iLen) = 0;
		i = iLen-1;
	} else
		i = strlen(pString)-1;

   while (i >= 0)
   {
      if ((pString[i] > ' ') && (pString[i] <= '~'))
         break;
      pString[i] = 0;
      i--;
   }

   return i+1;
}

/********************************* myTrim() *********************************
 *
 * Remove trailing spaces and tabs and other non-printables
 * Return pointer to result string
 *
 ****************************************************************************/

char *myTrim(char *pString)
{
   int i;

   // 
   i = strlen(pString)-1;
   while (i >= 0)
   {
      if ((pString[i] > ' ') && (pString[i] <= '~'))
         break;
      pString[i] = 0;
      i--;
   }

   return pString;
}

char *myTrim(char *pString, int iLen)
{
   int   i;

   *(pString + iLen) = 0;
   if (iLen < 2)
      return pString;

   // remove trailing spaces and tabs and other non-printables
   i = iLen-1;
   while (i >= 0)
   {
      if ((pString[i] > ' ') && (pString[i] <= '~'))
         break;
      pString[i] = 0;
      i--;
   }

   return pString;
}

char *myLTrim(char *pString, int iLen)
{
   char  *pTmp;

   pTmp = pString;
   if (iLen > 0)
      *(pTmp + iLen) = 0;

   while (*pTmp && *pTmp == ' ')
      pTmp++;

   strcpy(pString, pTmp);
   return pString;
}

/******************************** myBTrim() ********************************
 *
 * Remove both leading and trailing spaces
 *
 ***************************************************************************/

char *myBTrim(char *pString, int iLen)
{
   char *pTmp;
   int   i;

   if (iLen > 0)
      *(pString+iLen) = 0;

   // Remove leading space
   pTmp = pString;
   while (*pTmp && *pTmp <= ' ')
      pTmp++;

   // Remove trailing space
   i = strlen(pTmp)-1;
   while (i >= 0)
   {
      if ((pTmp[i] > ' ') && (pTmp[i] <= '~'))
         break;
      pTmp[i] = 0;
      i--;
   }

   strcpy(pString, pTmp);
   return pString;
}

/******************************** isPrintable() *****************************
 *
 ****************************************************************************/

int isPrintable(char *pBuf, int iLen)
{
   int iRet;

   for (iRet = 0; iRet < iLen; iRet++)
   {
      if (*pBuf < ' ' || *pBuf > 'z')
         break;
      pBuf++;
   }
   return iRet;
}

/******************************** isBlank() ********************************/

bool isBlank(LPCSTR pStr, int iLen)
{
   int  iTmp, iIdx;
   bool bRet = true;

   if (iLen == 0)
      iTmp = strlen(pStr);
   else
      iTmp = iLen;

   for (iIdx = 0; iIdx < iTmp; iIdx++)
   {
      if (*pStr != ' ')
      {
         bRet = false;
         break;
      }
      pStr++;
   }

   return bRet;
}

/*****************************************************************************/

char isChar(LPCSTR pBuf, int iLen)
{
   int   iIdx;
   char  cChar = ' ';

   for (iIdx = 0; iIdx < iLen; iIdx++)
   {
      if (*pBuf >= 'A' && *pBuf <= 'Z')
      {
         cChar = *pBuf;
         break;
      }
      pBuf++;
   }
   return cChar;
}

/*****************************************************************************/

bool isNumber(LPSTR pNumber)
{
   bool bRet=false;

   while (*pNumber)
   {
      if (isdigit(*pNumber) || *pNumber == '-')
         bRet = true;
      else
      {
         bRet = false;
         break;
      }
      pNumber++;
   }

   return bRet;
}

bool isNumber(LPSTR pNumber, int iLen)
{
   int   iCnt;
   bool  bRet=false;
   char  *pTmp = pNumber;

   for (iCnt = 0; iCnt < iLen; iCnt++)
   {
      if (isdigit(*pTmp))
         bRet = true;
      else
      {
         bRet = false;
         break;
      }
      pTmp++;
   }

   return bRet;
}

/******************************** blankPad() *******************************/

void blankPad(char *pBuf, int iLen)
{
   char *pTmp;
   int   iTmp=0;

   pTmp = pBuf;
   while (*pTmp && iTmp < iLen)
   {
      pTmp++;
      iTmp++;
   }
   while (iTmp++ < iLen)
      *pTmp++ = ' ';
}

// Terminate output string by zero
void blankPadz(char *pBuf, int iLen)
{
   char *pTmp;
   int   iTmp=0;

   pTmp = pBuf;
   while (*pTmp && iTmp < iLen)
   {
      pTmp++;
      iTmp++;
   }
   while (iTmp++ < iLen)
      *pTmp++ = ' ';
   *pTmp = 0;
}

// Remove CRLF before padding
void blankPad_RemCRLF(char *pBuf, int iLen)
{
   char *pTmp;
   int   iTmp=0;

   pTmp = pBuf;
   while (*pTmp && iTmp < iLen)
   {
      if (*pTmp == 10 || *pTmp == 13)
         *pTmp = ' ';
      pTmp++;
      iTmp++;
   }
   while (iTmp++ < iLen)
      *pTmp++ = ' ';
}

/******************************** blankRem() *******************************
 *
 * Reduce multi-space into single one
 * Return string length
 *
 ***************************************************************************/

int blankRem(char *pBuf, int iLen)
{
   char *pTmp, acTmp[8192];
   int   iTmp=0;
   bool  bSpace = false;

   if (!pBuf) return 0;
   if (iLen > 0)
      *(pBuf+iLen) = 0;

   // Remove leading space
   pTmp = pBuf;
   while (*pTmp == ' ')
      pTmp++;

   // Remove extra space in the middle
   while (*pTmp)
   {
      if (*pTmp == ' ')
      {
         if (!bSpace)
         {
            bSpace = true;
            acTmp[iTmp++] = ' ';
         }
      } else
      {
         acTmp[iTmp++] = *pTmp;
         bSpace = false;
      }

      pTmp++;
   }

   if (iTmp > 0 && acTmp[iTmp-1] <= ' ')
      iTmp--;
   acTmp[iTmp] = 0;
   strcpy(pBuf, acTmp);  
   return iTmp;
}

/******************************** blankRem() *******************************
 *
 * Reduce multi-space into single one
 * Special version to be used with CString input
 *
 ***************************************************************************/

int blankRem(CString& pStr)
{
   char *pTmp, acTmp[8192];
   int   iTmp=0;
   bool  bSpace = false;

   strcpy(acTmp, pStr);

   // Remove leading space
   pTmp = &acTmp[0];
   while (*pTmp == ' ')
      pTmp++;

   // Remove extra space in the middle
   while (*pTmp)
   {
      if (*pTmp == ' ')
      {
         if (!bSpace)
         {
            bSpace = true;
            acTmp[iTmp++] = ' ';
         }
      } else
      {
         acTmp[iTmp++] = *pTmp;
         bSpace = false;
      }

      pTmp++;
   }

   if (iTmp > 0 && acTmp[iTmp-1] <= ' ')
      iTmp--;
   acTmp[iTmp] = 0;
   pStr = acTmp;  
   return iTmp;
}

/******************************** blankRem() *******************************
 *
 * Reduce multi-space into single one
 * Also reduce blanks that preceeded or followed by checked char.
 *
 ***************************************************************************/

int blankRem(char *pBuf, char bChkChar, int iLen)
{
   char *pTmp, acTmp[8192];
   int   iTmp=0;
   bool  bSpace = false;

   if (iLen > 0)
      *(pBuf+iLen) = 0;

   // Remove leading space
   pTmp = pBuf;
   while (*pTmp == ' ')
      pTmp++;

   // Remove extra space in the middle
   while (*pTmp)
   {
      if (*pTmp == ' ')
      {
         if (!bSpace)
         {
            bSpace = true;
            acTmp[iTmp++] = ' ';
         }
      } else if ((*pTmp == bChkChar) && bSpace)
      {
         acTmp[iTmp-1] = *pTmp;
      } else
      {
         acTmp[iTmp++] = *pTmp;
         bSpace = false;
      }

      pTmp++;
   }

   if (iTmp > 0 && acTmp[iTmp-1] <= ' ')
      iTmp--;
   acTmp[iTmp] = 0;
   strcpy(pBuf, acTmp);  
   return iTmp;
}

/******************************** quoteRem() *******************************
 *
 * Remove all single and double quotes in string
 * Return number of quote removed.
 *
 ***************************************************************************/

int quoteRem(char *pBuf, int iLen)
{
   char *pTmp;
   int iTmp, iRemove=0, iRet;

   pTmp = pBuf;
   if (!iLen)
   {
      while (*pTmp)
      {
         if (*pTmp != 34 && *pTmp != 39)
            *(pBuf++) = *pTmp;
         else
            iRemove++;
         pTmp++;
      }
      iRet = iRemove;
      *pBuf = 0;
   } else
   {
      for (iTmp = 0; iTmp < iLen; iTmp++)
      {
         if (*pTmp != 34 && *pTmp != 39)
            *(pBuf++) = *pTmp;
         else 
            iRemove++;
         pTmp++;         
      }
      iRet = iRemove;
      while (iRemove-- > 0)
         *(pBuf++) = ' ';
   }

   return iRet;
}

/********************************** atoin() *********************************
 *
 * 06/01/2016 Modify to add remove comma option and chack for $.
 *
 ****************************************************************************/

int atoin(char *pStr, int iLen, bool bRemComma)
{
   char  acVal[32];
   int   iRet;

   if (!pStr || !*pStr)
      return 0;

   // Skip $
   if (*pStr == '$')
   {
      memcpy(acVal, pStr+1, iLen-1);
      acVal[iLen-1] = 0;
   } else
   {
      memcpy(acVal, pStr, iLen);
      acVal[iLen] = 0;
   }

   // Remove comma
   if (bRemComma)
      remChar(acVal, ',');

   iRet = atoi(acVal);
   return iRet;
}

ULONG atoln(char *pStr, int iLen, bool bRemComma)
{
   char  acVal[32];
   ULONG lRet;

   if (!pStr || !*pStr)
      return 0;

   // Skip $
   if (*pStr == '$')
   {
      memcpy(acVal, pStr+1, iLen-1);
      acVal[iLen-1] = 0;
   } else
   {
      memcpy(acVal, pStr, iLen);
      acVal[iLen] = 0;
   }

   // Remove comma
   if (bRemComma)
      remChar(acVal, ',');

   lRet = atol(acVal);
   return lRet;
}

/********************************** astol() *********************************/

long astol(char *pStr)
{
   char  acVal[32];
   long  lRet=0;

   if (!pStr || !*pStr)
      return 0;

   while (*pStr)
   {
      if (isdigit(*pStr))
         acVal[lRet++] = *pStr;
      pStr++;
   }
   acVal[lRet] = '\0';
   lRet = atol(acVal);
   return lRet;
}

/********************************** atofn() *********************************/

double atofn(char *pStr, int iLen, bool bChkSign)
{
   char     acVal[32], *pTmp;
   double   dRet;
   int      iTmp = 0, iTmp1 = 0;

   if (!pStr || !*pStr)
      return 0;

   pTmp = pStr;
   while (iTmp < iLen && *pTmp)
   {
      if (isdigit(*pTmp) || *pTmp == '.' || (bChkSign && *pTmp == '-'))
         acVal[iTmp1++] = *pTmp;
      iTmp++;
      pTmp++;
   }

   acVal[iTmp1] = '\0';
   try
   {
      dRet = atof(acVal);
   } catch (...)
   {
      dRet = 0;
   }
   return dRet;
}

/********************************** isValidYMD *******************************
 *
 * Check for YYYYMMDD format
 *
 *****************************************************************************/

bool isValidYMD(char *pDate, bool bChkDate, bool bChkYear, bool bChkToday)
{
   bool bRet=true;
   int  iTmp, iMonth, iYear, iDay, iCurYear;

   iCurYear = getCurYear();

   // Validate year
   iYear = atoin(pDate, 4);
   if (iYear < 1900 || (bChkYear && (iYear > iCurYear)))
      bRet = false;
   else
   {
      // Validate month
      iMonth = atoin(pDate+4, 2);
      if (iMonth < 1 || iMonth > 12)
         bRet = false;
      else if (bChkDate)
      {
         // Validate date
         iTmp = gdate_monthdays(iMonth, iYear);
         iDay = atoin(pDate+6, 2);
         if (iDay < 1 || iDay > iTmp)
            bRet = false;
         else if (bChkToday)
         {
            iTmp = atoin(pDate, 8);
            if (iTmp > m_iToday)
               bRet = false;
         }
      }
   }

   return bRet;
}

/********************************** strUpper() *********************************
 *
 * If string length is provided, output string will be padded with spaces and
 * not terminated with 0.
 *
 *******************************************************************************/

char *strUpper(char *pInStr, char *pOutStr, int iLen)
{
   char  *pIn, *pOut;
   int   iCnt;

   pIn = pInStr;
   pOut = pOutStr;

   if (iLen == 0)
   {
      while (*pIn)
      {
         if (isalpha(*pIn))
            *pOut++ = *pIn++ & 0x5F;
         else
            *pOut++ = *pIn++;
      }
      *pOut = 0;
   } else
   {
      for (iCnt = 0; iCnt < iLen; iCnt++)
      {
         if (isalpha(*pIn))
            *pOut++ = *pIn++ & 0x5F;
         else if (*pIn)
            *pOut++ = *pIn++;
         else
            *pOut++ = ' ';
      }
   }

   return pOutStr;
}

/********************************* appendTxtFile *****************************
 *
 * Append file1 to file2
 * If file1 doesn't exist, return -1.  Otherwise returns 0
 *
 *****************************************************************************/

int appendTxtFile(char *pFile1, char *pFile2)
{
   int   iRet;
   FILE  *fdIn, *fdOut;
   char  *pTmp, acTmp[4096];

   if (_access(pFile1, 0))
      return -1;

   fdIn = fopen(pFile1, "r");
   fdOut = fopen(pFile2, "a+");

   while (!feof(fdIn))
   {
      pTmp = fgets(acTmp, 4096, fdIn);
      if (pTmp)
         iRet = fputs(acTmp, fdOut);
      else
         break;
   }

   fclose(fdIn);
   fclose(fdOut);
   return 0;
}

/********************************* appendFixFile *****************************
 *
 * Append file1 to file2
 * Return number of records added
 *
 *****************************************************************************/

int appendFixFile(char *pFile1, char *pFile2, int iLen)
{
   int   iRet, iCnt=0;
   FILE  *fdIn, *fdOut;
   char  acTmp[4096];

   if (_access(pFile1, 0))
      return -1;

   fdIn = fopen(pFile1, "r");
   fdOut = fopen(pFile2, "a+");

   while (!feof(fdIn))
   {
      iRet = fread(acTmp, 1, iLen, fdIn);
      if (iRet == iLen)
         iRet = fwrite(acTmp, 1, iLen, fdOut);
      else
         break;
      iCnt++;
   }

   fclose(fdIn);
   fclose(fdOut);

   return iCnt;
}

/********************************** replChar() ******************************
 *
 * Replace a char in a string and drop CRLF.  Return number of replacements.
 *
 ****************************************************************************/

int replChar(char *pBuf, unsigned char cSrch, char cRepl)
{
   int iRet=0;

   if (cSrch == 10 || cSrch == 13)
   {
      while (*pBuf)
      {
         if ((unsigned char)*pBuf == cSrch)
         {
            *pBuf = cRepl;
            iRet++;
         }
         pBuf++;
      }
   } else
   {
      while (*pBuf && (*pBuf != 10 && *pBuf != 13))
      {
         if ((unsigned char)*pBuf == cSrch)
         {
            *pBuf = cRepl;
            iRet++;
         }
         pBuf++;
      }
   }
   if (*pBuf == 10 || *pBuf == 13)
      *pBuf = 0;

   return iRet;
}

/********************************** replChar() ******************************
 *
 * This version requires string length or string must ended with LF or NULL
 *
 ****************************************************************************/

int replChar(char *pBuf, unsigned char cSrch, char cRepl, int iLen)
{
   int iTmp, iRet=0;

   if (iLen > 0)
   {
      for (iTmp = 0; iTmp < iLen; iTmp++, pBuf++)
      {
         if ((unsigned char)*pBuf == cSrch)
         {
            *pBuf = cRepl;
            iRet = iTmp;
         }
      }
   } else
   {
      iTmp = 0;
      while (*pBuf && *pBuf != 10)  // scan till CR
      {
         if ((unsigned char)*pBuf == cSrch)
         {
            *pBuf = cRepl;
            iRet = iTmp;
         }
         pBuf++;
         iTmp++;
      }
   }

   return iRet;
}

int replUChar(unsigned char *pBuf, unsigned char cSrch, unsigned char cRepl, int iLen)
{
   int iTmp, iRet=0;

   if (iLen > 0)
   {
      for (iTmp = 0; iTmp < iLen; iTmp++, pBuf++)
      {
         if (*pBuf == cSrch)
         {
            *pBuf = cRepl;
            iRet = iTmp;
         }
      }
   } else
   {
      iTmp = 0;
      while (*pBuf && *pBuf != 10)  // scan till CR
      {
         if (*pBuf == cSrch)
         {
            *pBuf = cRepl;
            iRet = iTmp;
         }
         pBuf++;
         iTmp++;
      }
   }

   return iRet;
}

/********************************** replNull() ******************************
 *
 * This version requires string length or string must ended with LF or NULL
 *
 ****************************************************************************/

int replNull(char *pBuf, char cRepl, int iLen)
{
   int iTmp, iRet=0;

   if (iLen > 0)
   {
      for (iTmp = 0; iTmp < iLen; iTmp++, pBuf++)
      {
         if (!*pBuf)
         {
            *pBuf = cRepl;
            iRet = iTmp;
         }
      }
   } else
   {
      iTmp = 0;
      while (*pBuf != 10)  // scan till CR
      {
         if (!*pBuf)
         {
            *pBuf = cRepl;
            iRet = iTmp;
         }
         pBuf++;
         iTmp++;
      }
   }

   return iRet;
}

/******************************* replQuoteEx() *****************************
 *
 * Replace all char quotes and unprintable char with blank
 *
 ***************************************************************************/

void replQuoteEx(char *pBuf, int iLen)
{
   int iTmp, iRet=0;

   int iBuflen;

   if (!iLen)
      iBuflen = strlen(pBuf);
   else
      iBuflen = iLen;

   for (iTmp = 0; iTmp < iBuflen; iTmp++, pBuf++)
   {
      if (*pBuf < ' ' || *pBuf == 39)
      {
         *pBuf = ' ';
         iRet = iTmp;
      }
   }
}

/******************************* replCharEx() ******************************
 *
 * Replace all char less than or equal to searched char
 *
 ***************************************************************************/

int replCharEx(char *pBuf, char cSrch, char cRepl, int iLen)
{
   int iTmp, iRet=0;
   int iBuflen;

   if (!iLen)
      iBuflen = strlen(pBuf);
   else
      iBuflen = iLen;

   for (iTmp = 0; iTmp < iBuflen; iTmp++, pBuf++)
   {
      if (*pBuf <= cSrch)
      {
         *pBuf = cRepl;
         iRet = iTmp;
      }
   }
   return iRet;
}

/******************************* replNonNum() ******************************
 *
 * Replace all non-digit with specified char cRepl (default blank space).
 *
 ***************************************************************************/

int replNonNum(char *pBuf, char cRepl, int iLen)
{
   int iTmp, iRet=0;
   int iBuflen;

   if (!iLen)
      iBuflen = strlen(pBuf);
   else
      iBuflen = iLen;

   for (iTmp = 0; iTmp < iBuflen; iTmp++, pBuf++)
   {
      if (!isdigit(*pBuf))
      {
         *pBuf = cRepl;
         iRet = iTmp;
      }
   }
   return iRet;
}

/******************************* replCharEx() ******************************
 *
 * Scan strings for characters in specified character sets and replace them
 * with cRepl (default blank space). This version will remove extra space 
 * if bRemBlank=true.  
 *
 * Return 0 if nothing change, 1 if found in charset.
 *
 ***************************************************************************/

int replCharEx(char *pBuf, char *pCharSet, char cRepl, int iLen, bool bRemBlank, bool bReplUnPrt)
{
   char *pTmp, acTmp[2048];
   bool  bSpace = false;
   int   iRet=0;

   if (!pBuf || !*pBuf)
      return iRet;

   if (iLen > 0)
      *(pBuf+iLen) = 0;
   else if (strlen(pBuf) > 2047)
      return iRet;

   strcpy(acTmp, pBuf);
   pTmp = acTmp;
   if (bReplUnPrt)
   {
      while (*pTmp)
      {
         if (*pTmp < ' ' || (unsigned char)*pTmp > 126)  // ~
         {
            iRet = 2;
            *pTmp = cRepl;
         }
         pTmp++;
      }
   }

   pTmp = acTmp;
   while (pTmp)
   {
      pTmp = strpbrk(pTmp, pCharSet);
      if (pTmp)
      {
         iRet = 1;
         *pTmp = cRepl;
      }
   }

   if (bRemBlank && cRepl == ' ')
      blankRem(acTmp);
   strcpy(pBuf, acTmp);

   return iRet;
}

/****************************** replSqlChar() ******************************
 *
 * Replace all char which may caused problem to SQL search.
 *
 ***************************************************************************/

int replSqlChar(LPSTR pBuf, char cRepl, int iLen)
{
   int   iBuflen, iTmp, iRet=0;
   LPSTR pTmp = pBuf;  

   if (!iLen)
      iBuflen = strlen(pBuf);
   else
      iBuflen = iLen;

   for (iTmp = 0; iTmp < iBuflen; iTmp++, pTmp++)
   {
      if (*pTmp < ' ' || *pTmp == '\\' || (unsigned char)*pTmp > 126) //'~'
      {
         *pBuf++ = cRepl;
         iRet = iTmp;
      } else if (*pTmp != 39)    // Drop single quote
         *pBuf++ = *pTmp;
      else
         iRet = iTmp;
   }
   *pBuf = 0;

   return iRet;
}

/***************************** replBadChar() *******************************
 *
 * Replace all unprintable char and '\' .
 *
 ***************************************************************************/

int replBadChar(LPSTR pBuf, char cRepl, int iLen)
{
   int   iBuflen, iTmp, iRet=0;
   LPSTR pTmp = pBuf;  

   if (!iLen)
      iBuflen = strlen(pBuf);
   else
      iBuflen = iLen;

   for (iTmp = 0; iTmp < iBuflen; iTmp++, pBuf++)
   {
      if (*pBuf < ' ' || *pBuf == '\\' || (unsigned char)*pBuf > 126) //'~'
      {
         *pBuf = cRepl;
         iRet = iTmp;
      }
   }

   *pBuf = 0;

   return iRet;
}

/***************************** remJSChar() *********************************
 *
 * Remove Json sensitive char: backslash, double quote
 * Return position of char removed or replaced
 *
 ***************************************************************************/

int remJSChar(LPSTR pBuf, int iLen)
{
   int   iBuflen, iTmp, iRet=0;
   LPSTR pTmp = pBuf;  

   if (!iLen)
      iBuflen = strlen(pBuf);
   else
      iBuflen = iLen;

   for (iTmp = 0; iTmp < iBuflen; iTmp++, pTmp++)
   {
      // Remove double quote
      if (*pTmp < ' ' || *pTmp == '\\' || (unsigned char)*pTmp > 126) //'~'
      {
         // Replace backslash with space
         *pBuf++ = ' ';
         iRet = iTmp;
      } else if (*pTmp == '"')
      {
         // Remove double quote
         iRet = iTmp;
      } else
      {
         *pBuf++ = *pTmp;
      }
   }

   *pBuf = 0;

   return iRet;
}

/***************************** cleanOwner() ********************************
 *
 * Remove vertical bar, backslash, double quote 
 * Return length of output string
 *
 ***************************************************************************/

int cleanOwner(LPSTR pBuf, int iLen)
{
   int   iBuflen, iTmp, iRet=0;
   LPSTR pTmp = pBuf, pSave;  

   if (!iLen)
      iBuflen = strlen(pBuf);
   else
      iBuflen = iLen;

   pSave = pBuf;
   for (iTmp = 0; iTmp < iBuflen; iTmp++, pTmp++)
   {
      // Remove double quote
      if (*pTmp < ' ' || *pTmp == '|' || *pTmp == '\\' || (unsigned char)*pTmp > 126) //'~'
      {
         // Replace backslash with space
         *pBuf++ = ' ';
         iRet = iTmp;
      } else if (*pTmp == '"')
      {
         // Remove double quote
         iRet = iTmp;
      } else
      {
         *pBuf++ = *pTmp;
      }
   }

   *pBuf = 0;
   if (iRet > 0)
      iRet = blankRem(pSave);

   return iRet;
}

/**************************** replUnPrtChar() ******************************
 *
 * Replace all unprintable char with cRepl char
 *
 ***************************************************************************/

int replUnPrtChar(LPSTR pBuf, char cRepl, int iLen)
{
   int iTmp, iRet=0;
   int iBuflen;

   if (!iLen)
      iBuflen = strlen(pBuf);
   else
      iBuflen = iLen;

   for (iTmp = 0; iTmp < iBuflen; iTmp++, pBuf++)
   {
      if (*pBuf < ' ' || (unsigned char)*pBuf > 126) //'~'
      {
         *pBuf = cRepl;
         iRet = iTmp;
      }
   }
   return iRet;
}

int findUnPrtChar(LPSTR pBuf, int iLen)
{
   int iTmp, iRet=0;
   int iBuflen;

   if (!iLen)
      iBuflen = strlen(pBuf);
   else
      iBuflen = iLen;

   for (iTmp = 0; iTmp < iBuflen; iTmp++, pBuf++)
   {
      if (*pBuf < ' ' || (unsigned char)*pBuf > 126) //'~'
         iRet = iTmp;
   }
   return iRet;
}

/********************************** replStr() *******************************
 *
 * Replace a substring
 *
 ****************************************************************************/

int replStr(LPSTR pStr, LPCSTR pFind, LPCSTR pRepl)
{
   char  acTmp[4096], *pTmp;
   int   iRet=0,iLen;

   iLen = strlen(pFind);
   pTmp = strstr(pStr, pFind);
   if (pTmp)
   {
      *pTmp = 0;
      iRet = sprintf(acTmp, "%s%s%s", pStr, pRepl, pTmp+iLen);
      strcpy(pStr, acTmp);
   }

   return iRet;
}


/******************************* replStrAll() *******************************
 *
 * Replace all occurences of a substring
 *
 ****************************************************************************/

int replStrAll(LPSTR pStr, LPCSTR pFind, LPCSTR pRepl)
{
   char  acTmp[4096], *pTmp;
   int   iRet=0,iLen;

   iLen = strlen(pFind);
   pTmp = strstr(pStr, pFind);
   while (pTmp)
   {
      *pTmp = 0;
      iRet = sprintf(acTmp, "%s%s%s", pStr, pRepl, pTmp+iLen);
      strcpy(pStr, acTmp);
      pTmp = strstr(pStr, pFind);
   }

   return iRet;
}

/***************************** remExtraQuote() ******************************
 *
 * Remove all unprintable chars
 *
 ***************************************************************************/

int remExtraQuote(LPSTR pBuf)
{
   char  *pTmp = pBuf;
   bool  bQuote = false;
   int   iCnt = 0;

   while (*pBuf)
   {
      if (*pBuf != '"' || !bQuote)
      {
         *pTmp++ = *pBuf;
         if (*pBuf != '"')
            bQuote = false;
         else
            bQuote = true;
      } else
         iCnt++;

      pBuf++;
   }

   if (bQuote && !(iCnt % 2))
      *--pTmp = 0;
   else
      *pTmp = 0;

   return iCnt;
}

/***************************** remUnPrtChar() ******************************
 *
 * Remove all unprintable chars
 *
 ***************************************************************************/

int remUnPrtChar(LPSTR pBuf)
{
   char  *pTmp = pBuf;
   int   iCnt = 0;

   while (*pBuf)
   {
      if (*pBuf > 31 && (unsigned char)*pBuf < 126)
      {
         *pTmp++ = *pBuf;
         iCnt++;
      }
      pBuf++;
   }
   *pTmp = 0;

   return iCnt;
}

/********************************** remChar() *******************************
 *
 * Remove all occurences of a char in a string.  Return number of chars output.
 *
 ****************************************************************************/

int remChar(char *pBuf, char cSrch)
{
   char  *pTmp = pBuf;
   int   iCnt = 0;

   while (*pBuf)
   {
      if (*pBuf != cSrch)
      {
         *pTmp++ = *pBuf;
         iCnt++;
      }
      pBuf++;
   }
   *pTmp = 0;

   return iCnt;
}

int remChar(char *pBuf, char cSrch, int iBufLen)
{
   char  *pTmp = pBuf;
   int   iCnt = 0, iTmp;

   for (iTmp = 0; iTmp < iBufLen; iTmp++)
   {
      if (*pBuf != cSrch)
      {
         *pTmp++ = *pBuf;
         iCnt++;
      }
      pBuf++;
   }

   iTmp = iCnt;
   while (iTmp++ < iBufLen)
      *pTmp++ = ' ';

   return iCnt;
}

int remUChar(unsigned char *pBuf, unsigned char cSrch, int iBufLen)
{
   unsigned char  *pTmp = pBuf;
   int   iCnt = 0, iTmp;

   for (iTmp = 0; iTmp < iBufLen; iTmp++)
   {
      if (*pBuf != cSrch)
      {
         *pTmp++ = *pBuf;
         iCnt++;
      }
      pBuf++;
   }

   iTmp = iCnt;
   while (iTmp++ < iBufLen)
      *pTmp++ = ' ';

   return iCnt;
}

/******************************** remCharEx() ******************************
 *
 * Scan strings for characters in specified character sets and remove them.
 *
 ***************************************************************************/

void remCharEx(char *pBuf, char *pCharSet)
{
   char *pTmp, *pStart, acTmp[4096], acOut[4096];
   bool  bSpace = false;

   if (!pBuf || !*pBuf)
      return;

   if (strlen(pBuf) > 4095)
      return;

   strcpy(acTmp, pBuf);
   pTmp = acTmp;
   acOut[0] = 0;
   while (pTmp)
   {
      pStart = pTmp;
      pTmp = strpbrk(pTmp, pCharSet);
      if (pTmp)
         *pTmp++ = 0;
      strcat(acOut, pStart);
   }

   strcpy(pBuf, acOut);
}

/******************************* remDupWords ********************************
 *
 * Remove duplicate words in a string
 *
 *****************************************************************************/

void remDupWords(LPSTR pStr)
{
    char *pTemp1, *pTemp, sTmp[1024];

    pTemp1 = sTmp;
    pTemp = strtok(pStr, " ");

    if (pTemp != NULL)
    {
        strcpy(pTemp1, pTemp);
        while ((pTemp = strtok(NULL, " ")) != NULL)
        {
            if (strstr(pTemp1, pTemp) == NULL)
            {
                strcat(pTemp1, " ");
                strcat(pTemp1, pTemp);
            }
        }
        strcpy(pStr, pTemp1);
    } 
}

/******************************* remDupStr ***********************************
 *
 * Remove duplicate string in a delimited string
 *
 *****************************************************************************/

void remDupStr(LPSTR pStr, LPCSTR pDel)
{
    char *pTmp, *pTemp1, *pTemp, sTmp[1024];
    int  iTmp;

    pTemp1 = sTmp;
    pTemp = strtok(pStr, pDel);

    if (pTemp != NULL)
    {
        strcpy(pTemp1, pTemp);
        while ((pTemp = strtok(NULL, pDel)) != NULL)
        {
            if ((pTmp = strstr(pTemp1, pTemp)) == NULL)
            {
                strcat(pTemp1, pDel);
                strcat(pTemp1, pTemp);
            } else 
            {
               iTmp = strlen(pTemp);
               if (*(pTmp+iTmp) > ' ' && *(pTmp+iTmp) != *pDel)
               {
                  strcat(pTemp1, pDel);
                  strcat(pTemp1, pTemp);
               }
            }
        }
        strcpy(pStr, pTemp1);
    } 
}

/********************************* replByte_EQ *******************************
 *
 * Replace all occurences of a character with another one in a file.  If bExact
 * is set, only matched byte is replaced.  If not, all bytes equal or smaller
 * will be replaced with blank.
 *
 *****************************************************************************/

long replByte_EQ(LPCSTR pInfile, LPCSTR pOutfile, char bSrchChar, char bReplChar)
{
   int      iTmp;
   long     lCnt=0;
   unsigned long   nBytesWritten, nBytesRead, iLen;
   BOOL     bRet;
   HANDLE   fhIn, fhOut;
   char     acBuf[8192];

   iLen = 8192;

   // Open input file
   fhIn = CreateFile(pInfile, GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING,
          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

   if (fhIn == INVALID_HANDLE_VALUE)
      return -1;

   // Open Output file
   fhOut = CreateFile(pOutfile, GENERIC_WRITE, 0, NULL, CREATE_ALWAYS,
          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

   if (fhOut == INVALID_HANDLE_VALUE)
      return -2;

   while (ReadFile(fhIn, acBuf, iLen, &nBytesRead, NULL))
   {
      if (!nBytesRead)
         break;

      iTmp = 0;
      while (iTmp < nBytesRead)
      {
         if (acBuf[iTmp] == bSrchChar)
            acBuf[iTmp] = bReplChar;

         iTmp++;
      }

      bRet = WriteFile(fhOut, acBuf, nBytesRead, &nBytesWritten, NULL);
      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);
   }

   if (fhOut)
      CloseHandle(fhOut);
   if (fhIn)
      CloseHandle(fhIn);

   return lCnt;
}

/*****************************************************************************/

long replByte_LE(LPCSTR pInfile, LPCSTR pOutfile, char bSrchChar, char bReplChar)
{
   int      iTmp;
   long     lCnt=0;
   unsigned long   nBytesWritten, nBytesRead, iLen;
   BOOL     bRet;
   HANDLE   fhIn, fhOut;
   char     acBuf[8192];

   iLen = 8192;

   // Open input file
   fhIn = CreateFile(pInfile, GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING,
          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

   if (fhIn == INVALID_HANDLE_VALUE)
      return -1;

   // Open Output file
   fhOut = CreateFile(pOutfile, GENERIC_WRITE, 0, NULL, CREATE_ALWAYS,
          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

   if (fhOut == INVALID_HANDLE_VALUE)
      return -2;

   while (ReadFile(fhIn, acBuf, iLen, &nBytesRead, NULL))
   {
      if (!nBytesRead)
         break;

      iTmp = 0;
      while (iTmp < nBytesRead)
      {
         if (acBuf[iTmp] <= bSrchChar)
            acBuf[iTmp] = bReplChar;

         iTmp++;
      }

      bRet = WriteFile(fhOut, acBuf, nBytesRead, &nBytesWritten, NULL);
      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);
   }

   if (fhOut)
      CloseHandle(fhOut);
   if (fhIn)
      CloseHandle(fhIn);

   return lCnt;
}

/********************************* replaceByte *******************************
 *
 * Replace all occurences of a character with another one in a file.  It also
 * replaces double quotes with blanks.
 *
 *****************************************************************************/

long replaceByte(LPCSTR pInfile, char bSrchChar, char bReplChar, int iRecSize)
{
   int      iTmp;
   long     lCnt=0;
   unsigned long   nBytesWritten, nBytesRead, iLen;
   BOOL     bRet;
   HANDLE   fhIn, fhOut;
   char     acBuf[8192], acTmpFile[128], *pTmp;

   // Prepare temp file
   strcpy(acTmpFile, pInfile);
   pTmp = strrchr(acTmpFile, '.');
   if (pTmp)
      strcpy(pTmp, ".rpl");
   else
      strcat(acTmpFile, ".rpl");

   // Remove temp file is exist
   if (!_access(acTmpFile, 0))
      remove(acTmpFile);

   rename(pInfile, acTmpFile);

   fhIn = CreateFile(acTmpFile, GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING,
          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

   if (fhIn == INVALID_HANDLE_VALUE)
      return 3;

   // Open Output file
   fhOut = CreateFile(pInfile, GENERIC_WRITE, 0, NULL, CREATE_ALWAYS,
          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

   if (fhOut == INVALID_HANDLE_VALUE)
      return 4;

   if (iRecSize > 0)
      iLen = iRecSize;
   else
      iLen = 4096;

   while (ReadFile(fhIn, acBuf, iLen, &nBytesRead, NULL))
   {
      if (!nBytesRead)
         break;

      iTmp = 0;
      while (iTmp < nBytesRead)
      {
         if (acBuf[iTmp] == bSrchChar)
            acBuf[iTmp] = bReplChar;
         else if (acBuf[iTmp] == '\"')
            acBuf[iTmp] = ' ';

         iTmp++;
      }

      bRet = WriteFile(fhOut, acBuf, nBytesRead, &nBytesWritten, NULL);
      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);
   }

   if (fhOut)
      CloseHandle(fhOut);
   if (fhIn)
      CloseHandle(fhIn);

   return lCnt;
}

/********************************* getDate() ********************************
 *
 * Return current date + number of extended day.
 *
 ****************************************************************************/

//void getDate(char *pDate, long lExtend)
//{
//   CTime today = CTime::GetCurrentTime();
//   CTimeSpan tsExtend(lExtend);
//
//   today += tsExtend;
//   strcpy(pDate, today.Format("%Y%m%d"));
//}


void getCurDate(char *pDate)
{
   SYSTEMTIME sSysTime;

   GetLocalTime(&sSysTime);
   if (pDate)
      sprintf(pDate, "%.4d%.2d%.2d", sSysTime.wYear, sSysTime.wMonth, sSysTime.wDay);
}

/*****************************************************************************/

int getCurYear(char *pYear)
{
   SYSTEMTIME sSysTime;

   GetLocalTime(&sSysTime);
   if (pYear)
      sprintf(pYear, "%.4d", sSysTime.wYear);
   return  sSysTime.wYear;
}

/*******************************************************************
 *
 * 1 = DD-MMM-YYYY   21-JUL-2005
 * 2 = MM/DD/YYYY    7/21/2005 0:00:00 or the like
 * 3 = mmm dd yyyy   Nov 10 2005 0:00:00
 * 4 = MM/DD/YY      07/21/05
 * 5 = MMDDYY        072105
 * 6 = YY2YYYY       99 or 02
 * 7 = YY/MM/DD      97/12/31
 * 8 = YYMMDD        971231
 * 9 = YYYY-MM-DD    1993-08-31 00:00:00
 * 10= MMDDYYYY      12102011
 * default = MM/DD/YYYY
 *
 * if Expected Year = 0, use current year for verification
 *
 *******************************************************************/

char *dateConversion(char *pFromDate, char *pToDate, int iFromFmt, int iExpYear)
{
   char  *pRet, *pTmp, acTmp[32];
   int   i, iMon, iDay, iYear, iDate, iCurYear;
   bool  bChkYear;

   pRet = NULL;
   strcpy(pToDate, "        ");
   if (!pFromDate || !*pFromDate)
      return pRet;

   if (iExpYear > 0)
   {
      bChkYear = false;
      iCurYear = iExpYear;
   } else
   {
      bChkYear = true;
      iCurYear = getCurYear(acTmp);
   }

   switch (iFromFmt)
   {
      case DD_MMM_YYYY:       // 1
         for (i = 0; i < 12; i++)
            if (!memcmp(pFromDate+3, asMonth[i], 3))
               break;
         if (i < 12)
         {
            sprintf(pToDate, "%.4s%0.2d%.2s", pFromDate+7, i+1, pFromDate);
            pRet = pToDate;
         }
         break;

      case MM_DD_YYYY_1:      // 2 = m/d/yyyy
         strncpy(acTmp, pFromDate, 10);
         acTmp[10] = 0;
         pTmp = strchr(acTmp, ' ');
         if (pTmp) *pTmp = 0;
         iMon = atol(acTmp);
         if (iMon == 0) iMon = 1;
         pTmp = strchr(acTmp, '/');
         if (pTmp)
         {
            pTmp++;
            iDate = atol(pTmp);
            if (iDate == 0) iDate = 1;
            pRet = strchr(pTmp, '/');
            if (pRet)
            {
               pRet++;
               iYear = atoi(pRet);
               sprintf(pToDate, "%.4d%0.2d%0.2d", iYear, iMon, iDate);
               pRet = pToDate;
            }
         }
         break;

      case MM_DD_YYYY_2:      // 11 = mm/dd/yyyy
         sprintf(pToDate, "%.4s%.2s%.2s", pFromDate+6, pFromDate, pFromDate+3);
         pRet = pToDate;
         break;

      case MMM_DD_YYYY:       // 3 = Feb 01, 2016 or Feb 01 2016
         for (i = 0; i < 12; i++)
            if (!_memicmp(pFromDate, asMonth[i], 3))
               break;
         if (i < 12)
         {
            if (*(pFromDate+7) == ' ')
               sprintf(pToDate, "%.4s%0.2d%0.2d", pFromDate+8, i+1, atoin(pFromDate+4, 2));
            else
               sprintf(pToDate, "%.4s%0.2d%0.2d", pFromDate+7, i+1, atoin(pFromDate+4, 2));
            pRet = pToDate;
         }
         break;

      case MMDDYY1:           // 4 = mm/dd/yy
         strncpy(acTmp, pFromDate, 8);
         acTmp[8] = 0;
         pTmp = strchr(acTmp, ' ');
         if (pTmp) *pTmp = 0;
         iMon = atol(acTmp);
         // TCB If 00 set it to 01
         if (iMon == 0) iMon = 1;
         pTmp = strchr(acTmp, '/');
         if (pTmp)
         {
            pTmp++;
            iDate = atol(pTmp);
            // TCB If 00 set it to 01
            if (iDate == 0) iDate = 1;
            pRet = strchr(pTmp, '/');
            if (pRet)
            {
               pRet++;
               // Check for year with extra digit
               if (strlen(pRet) > 2)
                  *(pRet+2) = 0;
               i = atol(pRet);
               if (i+2000 > iCurYear)
                  sprintf(pToDate, "19%.2d%0.2d%0.2d", i, iMon, iDate);
               else
                  sprintf(pToDate, "20%.2d%0.2d%0.2d", i, iMon, iDate);
               pRet = pToDate;
            }
         }
         break;

      case MMDDYY2:           // 5 = mmddyy
         sprintf(acTmp, "%.6d", atoin(pFromDate, 6));
         i = atoin(&acTmp[4], 2);
         if (i+2000 > iCurYear)
            sprintf(pToDate, "19%.2d%.4s", i, acTmp);
         else
            sprintf(pToDate, "20%.2d%.4s", i, acTmp);
         pRet = pToDate;
         break;

      case YY2YYYY:           // 6
         i = atoin(pFromDate, 2);
         if (i+2000 > iCurYear)
            sprintf(pToDate, "19%.2d", i);
         else
            sprintf(pToDate, "20%.2d", i);
         pRet = pToDate;
         break;

      case YYMMDD1:           // 7
         if (*(pFromDate+2) == '/' && *(pFromDate+5) == '/')
         {
            i = atoin(pFromDate, 2);
            if (i+2000 > iCurYear)
               sprintf(pToDate, "19%.2s%.2s%.2s", pFromDate, pFromDate+3, pFromDate+6);
            else
               sprintf(pToDate, "20%.2s%.2s%.2s", pFromDate, pFromDate+3, pFromDate+6);
            pRet = pToDate;
         }
         break;

      case YYMMDD2:           // 8
         i = atoin(pFromDate, 2);
         if (i+2000 > iCurYear)
            sprintf(pToDate, "19%.2d%.4s", i, pFromDate+2);
         else
            sprintf(pToDate, "20%.2d%.4s", i, pFromDate+2);
         pRet = pToDate;
         break;

      case YYYY_MM_DD:        // 9
         if (*(pFromDate+4) == '-' && *(pFromDate+7) == '-')
         {
            sprintf(pToDate, "%.4s%.2s%.2s", pFromDate, pFromDate+5, pFromDate+8);
            pRet = pToDate;
         }
         break;

      case MMDDYYYY:
         iMon = atoin(pFromDate, 2);
         iDay = atoin(pFromDate+2, 2);
         iYear =atoin(pFromDate+4, 4);
         if (iYear > 0 && iYear <= iCurYear)
         {
            sprintf(pToDate, "%d%.2d%.2d", iYear, iMon, iDay);
            pRet = pToDate;
         }
         break;

      default:
         strcpy(acTmp, pFromDate);
         pTmp = strchr(acTmp, ' ');
         if (pTmp) *pTmp = 0;
         iMon = atol(acTmp);
         pTmp = strchr(acTmp, '/');
         if (pTmp)
         {
            pTmp++;
            iDate = atol(pTmp);
            pRet = strchr(pTmp, '/');
            if (pRet)
            {
               pRet++;
               iYear = atoi(pRet);
               if (iYear < 100)
               {
                  iYear += 2000;
                  if (iYear > iCurYear)
                     iYear -= 100;
               } else if (iYear < 1000)
               {
                  *(pRet+2) = 0;
                  iYear = atoi(pRet);
                  iYear += 2000;
                  if (iYear > iCurYear)
                     iYear -= 100;
               }
               sprintf(pToDate, "%d%0.2d%0.2d", iYear, iMon, iDate);
               pRet = pToDate;
            }
         }
         break;
   }

   if (pRet && strlen(pRet) > 8)
      *(pRet + 8) = 0;

   if (pRet && (iFromFmt != YY2YYYY) && !isValidYMD(pRet, true, bChkYear))
      pRet = NULL;

   return pRet;
}

/*****************************************************************************
 *
 * Remove $ and comma from a number
 * Return string length
 *
 *****************************************************************************/

int dollar2Num(char *pDollar, char *pNumStr)
{
   int iRet = 0;

   *pNumStr = 0;
   while (*pDollar)
   {
      if (*pDollar >= '.' &&  *pDollar <= '9')
      {
         iRet++;
         *pNumStr++ = *pDollar;
      }
      pDollar++;
   }
   *pNumStr = 0;
   return iRet;
}

/*****************************************************************************
 *
 * Remove $ and comma from a number
 * Return numerical value of string 
 *
 *****************************************************************************/

long dollar2Num(char *pDollar)
{
   int   iRet = 0;
   long  lRet = 0;
   char  sTmp[32];

   while (*pDollar)
   {
      if (*pDollar >= '.' &&  *pDollar <= '9')
      {
         sTmp[iRet++] = *pDollar;
      }
      pDollar++;
   }

   if (iRet > 0)
   {
      sTmp[iRet] = 0;
      lRet = atol(sTmp);
   }

   return lRet;
}

/*****************************************************************************
 *
 * Convert a number string to double
 *
 *****************************************************************************/

double dollar2Double(char *pValue)
{
   int    iRet = 0;
   double dRet = 0.0;
   char   sTmp[64], *pNumStr;

   pNumStr = sTmp;
   while (*pValue)
   {
      if (*pValue >= '-' &&  *pValue <= '9')
      {
         iRet++;
         *pNumStr++ = *pValue;
      }
      pValue++;
   }
   *pNumStr = 0;

   if (iRet > 9)
      dRet = 0;
   if (iRet > 0)
      dRet = atof(sTmp);

   return dRet;
}

/*****************************************************************************/

int csv2fix(char *pInfile, char *pOutfile, char *pRecDef)
{
   return 0;
}

/*****************************************************************************/

// Delete file(s), wild card accepted
// Return 0 if successful, -1 if file not exist
int delFile(char *pFilename)
{
   long     lHandle;
   int      iTmp, iRet=0;
   char     acTmp[256], acPath[256], *pTmp;
   struct   _finddata_t  sFileInfo;

   lHandle = _findfirst(pFilename, &sFileInfo);
   if (lHandle <= 0)
      return -1;

   // Store path name
   strcpy(acPath, pFilename);
   pTmp = strrchr(acPath, '\\');
   if (pTmp)
   {
      *(++pTmp) = 0;
      iTmp = 0;
      while (!iTmp)
      {
         sprintf(acTmp, "%s%s", acPath, sFileInfo.name);
         iRet |= remove(acTmp);

         // Find next file
         iTmp = _findnext(lHandle, &sFileInfo);
      }
   } else
   {
      remove(pFilename);
   }

   // Close handle
   _findclose(lHandle);

   return iRet;
}

/*****************************************************************************/

bool isValidMDY(char *pDate)
{
   bool bRet=true;
   int  iTmp;

   // Validate year
   iTmp = atoin(pDate+4, 4);
   if (iTmp < 1900 || iTmp > 2027)
      bRet = false;
   else
   {
      // Validate month
      iTmp = atoin(pDate, 2);
      if (iTmp < 1 || iTmp > 12)
         bRet = false;
      else
      {
         // Validate date
         iTmp = atoin(pDate+2, 2);
         if (iTmp < 1 || iTmp > 31)
            bRet = false;
      }
   }

   return bRet;
}

/*****************************************************************************/

LPSTR isCharIncluded(LPSTR pBuf, char cChar, int iLen)
{
   int   iRet, iCnt;
   LPSTR pRet=NULL;

   if (iLen > 0)
      iCnt = iLen;
   else
      iCnt = strlen(pBuf);

   for (iRet = 0; iRet < iCnt; iRet++)
   {
      if (*pBuf == cChar)
      {
         pRet = pBuf;
         break;
      }
      pBuf++;
   }
   return pRet;
}

/*****************************************************************************/

LPSTR isCharIncluded(LPSTR pBuf, int iLen)
{
   int   iRet, iCnt;
   LPSTR pRet=NULL;

   if (iLen > 0)
      iCnt = iLen;
   else
      iCnt = strlen(pBuf);

   for (iRet = 0; iRet < iCnt; iRet++)
   {
      if (isalpha(*pBuf))
      {
         pRet = pBuf;
         break;
      }
      pBuf++;
   }
   return pRet;
}

/*****************************************************************************/

LPSTR isTabIncluded(LPSTR pBuf, int iLen)
{
   int   iRet, iCnt;
   LPSTR pRet=NULL;

   if (iLen > 0)
      iCnt = iLen;
   else
      iCnt = strlen(pBuf);

   for (iRet = 0; iRet < iCnt; iRet++)
   {
      if (*pBuf == '\t')
      {
         pRet = pBuf;
         break;
      }
      pBuf++;
   }
   return pRet;
}

/*****************************************************************************/

// Return position of digit starts
LPSTR isNumIncluded(LPSTR pBuf, int iLen)
{
   int   iRet, iCnt;
   LPSTR pRet=NULL;

   if (iLen > 0)
      iCnt = iLen;
   else
      iCnt = strlen(pBuf);

   for (iRet = 0; iRet < iCnt; iRet++)
   {
      if (isdigit(*pBuf))
      {
         pRet = pBuf;
         break;
      }
      pBuf++;
   }
   return pRet;
}

/*****************************************************************************/

void DoValidDate(LPSTR pBuf, int iLen)
{
   int iRet;

   for (iRet = 0; iRet < iLen; iRet++)
   {
      if (*pBuf < '0' || *pBuf > '9')
         *pBuf = '0';
      pBuf++;
   }
}

/***************************************************************************
 *
 * Determines whether the year is a leap year
 *
 ***************************************************************************/

bool isLeapYear(long year)
{
   if ( ((year % 4) == 0) &&
        (((year % 100) != 0) || ((year % 400) == 0)))
   {
      return true;
   } else
   {
      return false;
   }
}

/***************************************************************************
 * Function: gdate_daytomy
 *
 * Purpose:
 *
 * converts day to d/m/y
 ***************************************************************************/

void gdate_daytodmy(long days, int *yrp, int *monthp, int *dayp)
{
   int years;
   int nleaps;
   int month;
   int mdays;

   /* get number of completed years and calc leap days */
   years = (int) (days / 365);
   days = days % 365;
   nleaps = (years / 4) - (years / 100) + (years / 400);

   while (nleaps > days)
   {
      days += 365;
      years--;
      nleaps = (years / 4) - (years / 100) + (years / 400);
   }

   days -= nleaps;

   /* add one year for current (non-complete) year */
   years++;

   /* current month */
   for (month = 0; month < 12; month++)
   {
      mdays = monthdays[month];
      if (isLeapYear(years) && (month == 1))
      {
         mdays++;
      }

      if (days == mdays)
      {
         days = 0;
         month++;
         break;
      } else if (days < mdays)
      {
         break;
      } else
      {
         days -= mdays;
      }
   }
   /* conv month from 0-11 to 1-12 */

   if (monthp != NULL)
   {
      *monthp = month+1;
   }

   if (dayp != NULL)
   {
      *dayp = (int) days + 1;
   }

   if (yrp != NULL)
   {
      *yrp = years;
   }
}

/***************************************************************************
 * Function: gdate_dmytoday
 *
 * Purpose:
 *
 * converts d/m/y to a day
 ***************************************************************************/

long gdate_dmytoday(int yr, int month, int day)
{
   int nleaps;
   int i;
   long ndays;

   /* exclude the current year */
   yr--;
   nleaps = (yr / 4) - (yr / 100) + (yr / 400);

   /* in any given year, day 0 is jan1 */
   month--;
   day--;
   ndays = 0;

   for (i = 0; i < month ; i++)
   {
      ndays += monthdays[i];
      if (isLeapYear(yr+1) && (i == 1))
      {
         ndays++;
      }
   }

   ndays = ndays + day + nleaps + (yr * 365L);
   return(ndays);
}

/***************************************************************************
 * Function: gdate_monthdays
 *
 * Purpose:
 *
 * Gets number of days in month
 ***************************************************************************/

int gdate_monthdays(int month, int year)
{
   int ndays;
   ndays = monthdays[month - 1];

   if (isLeapYear(year) && (month == 2))
   {
      ndays++;
   }

   return(ndays);
}

/***************************************************************************
 * Function: gdate_weekday
 *
 * Purpose:
 *
 * Gets the day of the week
 ***************************************************************************/

int gdate_weekday(long daynr)
{
   return((int) ((daynr + 1) % 7));
}

// Create a Julian date of the form YYYYDDD
// from a Gregorian date YYYYMMDD
long GregorianToJulian(LPCSTR pGDate, LPSTR pJDate)
{
   long JDate;
   long iMonth, iDay, iYear;

   iMonth = atoin((char *)(pGDate+4), 2);
   iDay   = atoin((char *)(pGDate+6), 2);
   iYear  = atoin((char *)pGDate, 4);
   JDate = 0;

   // Add in days for months already elapsed.
   for( int i = 0; i < iMonth - 1; ++i )
      JDate += monthdays[i];

   // Add in days for this month.
   JDate += iDay;

   // Check for leap year.
   if ((iYear % 100) != 0 && (iYear % 4) == 0 )
      JDate++;

   // Add in year.
   JDate += iYear*1000;
   if (pJDate)
      sprintf(pJDate, "%ld", JDate);

   return JDate;
}

// Convert CCYYDDD to YYYYMMDD
long JulianToGregorian(LPCSTR pJDate, LPSTR pGDate)
{
   long  JDate;
   long  iMonth, iDay, iYear, lRet;

   JDate = atoin((char *)(pJDate+4), 3);
   if (!JDate)
      return 0;

   iYear = atoin((char *)pJDate, 4);
   for (iMonth=0, iDay=JDate; iMonth < 12; iMonth++)
   {
      if (iDay <= monthdays[iMonth])
         break;

      if (iMonth == 1 && isLeapYear(iYear))
      {
         if (iDay == monthdays[iMonth]+1)
            break;

         iDay--;
      }

      iDay -= monthdays[iMonth];
   }

   iMonth++;
   lRet = iYear*10000 + iMonth*100 + iDay;
   if (pGDate)
      sprintf(pGDate, "%ld", lRet);

   return lRet;
}

/*****************************************************************************
 *
 * GetNextMonth()
 *
 ****************************************************************************/

void getNextMonth(char *pBuf, int iMonths)
{
   SYSTEMTIME  curSystemTime;

   GetLocalTime(&curSystemTime);

   if (curSystemTime.wMonth >= 12)
   {
      curSystemTime.wMonth = 1;
      curSystemTime.wYear += 1;
   } else
      curSystemTime.wMonth += 1;

   // Calculate leap year
   if (curSystemTime.wMonth == 2 && curSystemTime.wDay > 28)
   {
      if (isLeapYear(curSystemTime.wYear))
         curSystemTime.wDay = 29;
      else
         curSystemTime.wDay = 28;
   }

   sprintf(pBuf, "%.4d%.2d%.2d", curSystemTime.wYear, curSystemTime.wMonth, curSystemTime.wDay);
}

/*****************************************************************************
 *
 *
 ****************************************************************************/

void getPrevMonth(char *pBuf, int iMonths)
{
   SYSTEMTIME  curSystemTime;

   GetLocalTime(&curSystemTime);

   if (iMonths > 11)
   {
      *pBuf = 0;
      return;
   }

   if (curSystemTime.wMonth <= iMonths)
   {
      curSystemTime.wMonth = (curSystemTime.wMonth+12)-iMonths;
      curSystemTime.wYear -= 1;
   } else
      curSystemTime.wMonth -= iMonths;

   // Calculate leap year
   if (curSystemTime.wMonth == 2 && curSystemTime.wDay > 28)
   {
      if (isLeapYear(curSystemTime.wYear))
         curSystemTime.wDay = 29;
      else
         curSystemTime.wDay = 28;
   }

   sprintf(pBuf, "%.4d%.2d%.2d", curSystemTime.wYear, curSystemTime.wMonth, curSystemTime.wDay);
}

/*****************************************************************************
 *
 * Return month number.  If buffer provided, return month abbr in it.
 *
 ****************************************************************************/

int getCurMonth(char *pBuf)
{
   SYSTEMTIME  curSystemTime;
   int         iRet;

   GetLocalTime(&curSystemTime);

   iRet = curSystemTime.wMonth;
   if (pBuf)
      strcpy(pBuf, asMonth[iRet-1]);

   return iRet;
}

/*****************************************************************************
 *
 * If iMonth is 0, return current month.
 *
 ****************************************************************************/

char *getMonthAbbr(int iMonth)
{
   char *pRet;

   if (iMonth > 12 || iMonth < 0)
      pRet = NULL;
   else if (iMonth > 0)
      pRet = asMonth[iMonth-1];
   else
   {
      int iRet;
      iRet = getCurMonth();
      pRet = asMonth[iRet-1];
   }

   return pRet;
}

/*****************************************************************************
 *
 * Move GrGr files to its subfolder.
 *
 ****************************************************************************/

void fixDec(char *pStr, int iPos)
{
   char acTmp[32], *pTmp;

   pTmp = pStr;
   while (*pTmp == ' ')
      pTmp++;
   memcpy(acTmp, pTmp, iPos);
   acTmp[iPos] = '.';
   strcpy((char *)&acTmp[iPos+1], pTmp+iPos);
   strcpy(pStr, acTmp);
}

/*****************************************************************************
 *
 * Set file time
 *
 ****************************************************************************/

void touch(char *pFilename)
{
   HANDLE      hFile;
   FILETIME    ft;
   SYSTEMTIME  st;

   hFile = CreateFile(pFilename, GENERIC_WRITE, 0, NULL, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, NULL);
   GetSystemTime(&st);                 // gets current time
   SystemTimeToFileTime(&st, &ft);     // converts to file time format
   SetFileTime(hFile, &ft, &ft, &ft);  // sets last-write time for file
   CloseHandle(hFile);
}

/*****************************************************************************
 *
 * Use time of file1 to set file2
 *
 ****************************************************************************/

int touch(LPCSTR pFile1, LPCSTR pFile2)
{
   HANDLE      hFile1, hFile2;
   FILETIME    ftCreate, ftLastAccess, ftLastWrite;

   if (_access(pFile1, 0) || _access(pFile1, 0))
      return -1;
   hFile1 = CreateFile(pFile1, GENERIC_WRITE, 0, NULL, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, NULL);
   hFile2 = CreateFile(pFile2, GENERIC_WRITE, 0, NULL, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, NULL);
   GetFileTime(hFile1, &ftCreate, &ftLastAccess, &ftLastWrite);  // gets last-write time for file
   SetFileTime(hFile2, &ftLastWrite, &ftLastWrite, &ftLastWrite);  // sets last-write time for file
   CloseHandle(hFile1);
   CloseHandle(hFile2);

   return 0;
}

/********************************* isTextFile *******************************
 *
 * Test first 2048 bytes for CR or LF.  Return TRUE if it seems to be text file
 * with CRLF.  FALSE if no CRLF or NULL character found.
 *
 *****************************************************************************/

bool isTextFile(LPCSTR pInfile, int iLineLen)
{
   int      iTmp, iLen;
   unsigned long   nBytesRead;
   HANDLE   fhIn;
   char     acBuf[8192];
   bool     bRet = false;

   // Open input file
   fhIn = CreateFile(pInfile, GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING,
          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

   if (fhIn == INVALID_HANDLE_VALUE)
      return false;

   iLen = iLineLen;
   if (iLen > 8192)
      iLen = 8192;

   if (ReadFile(fhIn, acBuf, iLen, &nBytesRead, NULL))
   {
      iTmp = 0;
      while (iTmp < nBytesRead)
      {
         if (acBuf[iTmp] == 13 || acBuf[iTmp] == 10)
         {
            bRet = true;
            break;
         } else if (!acBuf[iTmp] || acBuf[iTmp] > 127)
            break;

         iTmp++;
      }
   }

   if (fhIn)
      CloseHandle(fhIn);

   return bRet;
}

/******************************* isTabEmbeded ********************************
 *
 * Test first 2048 bytes for tab character.  Return TRUE if tab is found.
 * FALSE if not
 *
 *****************************************************************************/

bool isTabEmbeded(LPCSTR pInfile)
{
   int      iTmp;
   unsigned long   nBytesRead;
   HANDLE   fhIn;
   char     acBuf[2048];
   bool     bRet = false;

   // Open input file
   fhIn = CreateFile(pInfile, GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING,
          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

   if (fhIn == INVALID_HANDLE_VALUE)
      return false;

   bRet = false;
   if (ReadFile(fhIn, acBuf, 2048, &nBytesRead, NULL))
   {
      iTmp = 0;
      while (iTmp < nBytesRead)
      {
         if (acBuf[iTmp] == 9)
         {
            bRet = true;
            break;
         } else if (acBuf[iTmp] == 13 || acBuf[iTmp] == 10)
            break;
         iTmp++;
      }
   }

   if (fhIn)
      CloseHandle(fhIn);

   return bRet;
}

/********************************* guessRecLen *******************************
 *
 * Test first 8192 bytes for CR or LF.  If there is LF after expected line length,
 * return iLineLen+1.  If CRLF found, return iLineLen+2, else return iLineLen.
 *
 *****************************************************************************/

int guessRecLen(LPCSTR pInfile, int iLineLen)
{
   int      iRet, iLen;
   unsigned long   nBytesRead;
   HANDLE   fhIn;
   char     acBuf[8192];

   if (!iLineLen)
      return 0;

   // Open input file
   fhIn = CreateFile(pInfile, GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING,
          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

   if (fhIn == INVALID_HANDLE_VALUE)
      return 0;

   iLen = iLineLen;
   if (iLen > 8192)
      iLen = 8192;

   iRet = iLineLen;
   if (ReadFile(fhIn, acBuf, iLen+2, &nBytesRead, NULL))
   {
      if (acBuf[iLineLen] == 10)
         iRet = iLineLen+1;
      else if (acBuf[iLineLen+1] == 10)
         iRet = iLineLen+2;
   }

   CloseHandle(fhIn);

   return iRet;
}

/****************************** AnsiToUnicode *********************************
 *
 * AnsiToUnicode converts the ANSI string pszA to a Unicode string
 * and returns the Unicode string through ppszW. Space for the
 * the converted string is allocated by AnsiToUnicode.
 *
 ******************************************************************************/

HRESULT __stdcall AnsiToUnicode(LPCSTR pszA, wchar_t *pszW, int iLen) 
{ 
   int cCharacters;
   DWORD dwError;

   *pszW = 0;

   // If input is null then just return the same.
   if (NULL == pszA)
      return NOERROR;

   // Determine number of wide characters to be allocated for the
   // Unicode string.
   cCharacters =  strlen(pszA);
   if (cCharacters > iLen)
      cCharacters = iLen;

   // Covert to Unicode.
   if (0 == MultiByteToWideChar(CP_ACP, 0, pszA, cCharacters, pszW, cCharacters))
   {
      dwError = GetLastError();
      return HRESULT_FROM_WIN32(dwError);
   }

   *(pszW+cCharacters) = 0;
   return NOERROR;
} 

/****************************** UnicodeToAnsi *********************************
 *
 * UnicodeToAnsi converts the Unicode string pszW to an ANSI string
 * and returns the ANSI string through ppszA. Space for the
 * the converted string is allocated by UnicodeToAnsi.
 *
 ******************************************************************************/

HRESULT __stdcall UnicodeToAnsi(wchar_t *pszW, LPSTR pszA, int iLen) 
{ 
   int   iChars;
   DWORD dwError;

   *pszA = 0;

   // If input is null then just return the same.
   if (pszW == NULL)
      return NOERROR;

   iChars = wcslen(pszW);
   if (iChars > iLen)
      iChars = iLen-1;

   // Convert to ANSI.
   if (0 == WideCharToMultiByte(CP_ACP, 0, pszW, iChars, pszA, iChars, NULL, NULL))
   {
      dwError = GetLastError();
      return HRESULT_FROM_WIN32(dwError);
   }

   *(pszA+iChars) = 0;
   return NOERROR;
}

/****************************** UnicodeToAnsi *********************************
 *
 * Convert the Windows Unicode file to ANSI text file.
 * 08/24/2011 Add code to check for UTF-8 format (VEN).
 * 09/15/2011 Fix bug that report error even though it is ok.
 *
 ******************************************************************************/

HRESULT __stdcall UnicodeToAnsi(LPCSTR pInfile, LPCSTR pOutfile) 
{ 
   CString  csText;
   CFile    cfInfile, cfOutfile;
   int      nFileLen;
   DWORD    dwError;

   LogMsgD("Converting Unicode to Ansi: %s", pInfile);
   if (!cfInfile.Open( pInfile, CFile::modeRead ) )
   {
      LogMsg("***** Error unable to open file: %s", pInfile);
      dwError = GetLastError();
      return HRESULT_FROM_WIN32(dwError);
   }
   nFileLen = (int)cfInfile.GetLength();

   // Allocate buffer for binary file data
   unsigned char* pBuffer = new unsigned char[nFileLen + 2];
   cfInfile.Read(pBuffer, 2);

   // Windows Unicode file is detected if starts with FEFF
   if (pBuffer[0] == 0xff && pBuffer[1] == 0xfe )
   {
      nFileLen -= 2;
      nFileLen = cfInfile.Read(pBuffer, nFileLen);
      pBuffer[nFileLen] = '\0';
      pBuffer[nFileLen+1] = '\0'; // in case 2-byte encoded
      // Contains byte order mark, so assume wide char content
      // non _UNICODE builds should perform UCS-2 (wide char) to UTF-8 conversion here
      csText = (LPCWSTR)(&pBuffer[0]);

      try
      {
         cfOutfile.Open(pOutfile, CFile::modeCreate|CFile::modeWrite|CFile::shareExclusive);
         cfOutfile.Write(csText, csText.GetLength());
         cfOutfile.Close();
         dwError = 0;
      }
      catch (CFileException e)
      {
         LogMsg("***** Error unable to create file: %s (%d)", pOutfile, e.m_cause);
         dwError = GetLastError();
      }
   } else if (pBuffer[0] == 0xef && pBuffer[1] == 0xbb )
   {
      // Throw away 3rd byte 0xbf
      cfInfile.Read(pBuffer, 1);
      nFileLen -= 3;
      nFileLen = cfInfile.Read(pBuffer, nFileLen);
      pBuffer[nFileLen] = '\0';
      pBuffer[nFileLen+1] = '\0'; // in case 2-byte encoded
      csText = (LPCSTR)(&pBuffer[0]);

      try
      {
         cfOutfile.Open(pOutfile, CFile::modeCreate|CFile::modeWrite|CFile::shareExclusive);
         cfOutfile.Write(csText, csText.GetLength());
         cfOutfile.Close();
         dwError = 0;
      }
      catch (CFileException e)
      {
         LogMsg("***** Error unable to create file: %s (%d)", pOutfile, e.m_cause);
         dwError = GetLastError();
      }
   } else
   {
      LogMsg("*** %s is not a Windows Unicode file.  No conversion.", pInfile);
      dwError = 1;
   }

   cfInfile.Close();
   delete [] pBuffer;

   return dwError;
}

/*****************************************************************************/

void memsetU(wchar_t *var, wchar_t wChar, int iLen)
{
   int   iCnt;
   for (iCnt = 0; iCnt < iLen; iCnt++)
      *var++ = wChar;
   *var = 0;
}

/*****************************************************************************/

void __stdcall GetCharW(wchar_t *var, LPSTR str, int varLen) 
{
   sprintf(str, "%.*S", varLen, var);
}

/*****************************************************************************/

void __stdcall SetCharW(wchar_t *var, LPSTR str, int varLen) 
{
   int iCnt;

   iCnt = swprintf(var, L"%.*S", varLen, str);
   if (varLen > iCnt) 
      memsetU(var+iCnt, L' ', varLen-iCnt);
}

/*****************************************************************************
 *
 * Desc: Get a string from a text file and check for even number of double quote.
 *       If not, keep reading.
 *
 * Return number of bytes read.  <0 if read error
 *     0: EOF
 *    -1: File not open, invalid buffer pointer, invalid max buffer length
 *    -2: Buffer is not big enough
 *
 *****************************************************************************/

int countQuotes(char *pBuf)
{
   int iRet = 0;

   while (*pBuf)
      if (*pBuf++ == '"')
         iRet++;

   return iRet;
}

int myGetStrEQ(char *pBuf, int iBufLen, FILE *fd)
{
   char  sTmp[2048];
   bool  bDone=false;
   int   iRet, iTmp, iLen;

   if (!fd || !pBuf || iBufLen <= 0)
      return -1;

   *pBuf = 0;
   iLen = 0;
   do 
   {
      if (fgets(sTmp, 2048, fd))
      {
         iTmp = strlen(sTmp);
         iLen += iTmp;     // New output len
         if (iLen > iBufLen)
         {
            iRet = -2;
            bDone = true;
         } else
         {
            // Remove LF to avoid break in the middle
            if (sTmp[iTmp-1] == '\n')
            {
               sTmp[iTmp-1] = 0;
               iLen--;
            }

            strcat(pBuf, sTmp);
            iRet = countQuotes(pBuf);
            if ((iRet % 2) == 0)
               bDone = true;
         }
      } else
      {
         bDone = true;
         iRet = 0;
      }
   } while (!bDone);

   return iRet;
}

/*****************************************************************************
 *
 * Desc: Get a string from a text file and check for double quote as last 
 *       character.
 *
 * Return number of bytes read.  <0 if read error
 *     0: EOF
 *    -1: File not open, invalid buffer pointer, invalid max buffer length
 *    -2: Buffer is not big enough
 *
 *****************************************************************************/

int myGetStrQC(char *pBuf, int iBufLen, FILE *fd)
{
   char  sTmp[2048];
   bool  bDone=false;
   int   iRet, iTmp, iLen;

   if (!fd || !pBuf || iBufLen <= 0)
      return -1;

   *pBuf = 0;
   iLen = 0;
   do 
   {
      if (fgets(sTmp, 2048, fd))
      {
         iTmp = strlen(sTmp);
         iLen += iTmp;     // New output len
         if (iLen > iBufLen)
         {
            iRet = -2;
            bDone = true;
         } else
         {
            if (sTmp[iTmp-1] == '\n')
            {
               sTmp[iTmp-1] = 0;
               iLen--;
            }

            strcat(pBuf, sTmp);
            iRet = iLen;
            if (sTmp[iTmp-1] == '"')
               bDone = true;
         }
      } else
      {
         bDone = true;
         iRet = 0;
      }
   } while (!bDone);

   return iRet;
}

/*****************************************************************************
 *
 * Desc: Get a string from a text file and check for double quote as last 
 *       character and previous char is not a separator.  It also check for
 *       null character embeded in string and replace it with space.
 *       Use this special version only when you know how it works.  Use in
 *       MergeMno.cpp
 *
 * Return number of bytes read.  <0 if read error
 *     0: EOF
 *    -1: File not open, invalid buffer pointer, invalid max buffer length
 *    -2: Buffer is not big enough
 *
 *****************************************************************************/

int myGetStrQC(LPSTR pBuf, char cSeparator, int iBufLen, FILE *fd)
{
   char  sTmp[2048];
   bool  bDone=false;
   int   iRet, iTmp, iLen;

   if (!fd || !pBuf || iBufLen <= 0)
      return -1;

   *pBuf = 0;
   iLen = 0;
   do 
   {
      if (fgets(sTmp, 2048, fd))
      {
#ifdef _DEBUG
         //if (!memcmp(&sTmp[1], "X0287319", 8) )
         //   iTmp = 0;
#endif
         // Remove null char in string
         for (iTmp=0; sTmp[iTmp] != '\n'; iTmp++)
         {
            if (!sTmp[iTmp])
               sTmp[iTmp] = ' ';
         }

         iTmp = strlen(sTmp);
         iLen += iTmp;     // New output len
         if (iLen > iBufLen)
         {
            iRet = -2;
            bDone = true;
         } else
         {
            if (sTmp[iTmp-1] == '\n')
            {
               sTmp[iTmp-1] = 0;
               iLen--;
               iTmp--;
            }

            strcat(pBuf, sTmp);
            iRet = iLen;
            // Avoid line ending with incomplete token
            if (sTmp[iTmp-1] == '"' && sTmp[iTmp-2] != cSeparator)
               bDone = true;
            // Or invalid token as in ""B""
            if (sTmp[iTmp-1] == '"' && sTmp[iTmp-2] == '"' && sTmp[iTmp-3] != cSeparator)
               bDone = false;
         }
      } else
      {
         bDone = true;
         iRet = 0;
      }
   } while (!bDone);

   return iRet;
}

/*****************************************************************************
 *
 * Desc: Get a string from a text file and check for digit as last character.
 *
 * Return number of bytes read.  <0 if read error
 *     0: EOF
 *    -1: File not open, invalid buffer pointer, invalid max buffer length
 *    -2: Buffer is not big enough
 *
 *****************************************************************************/

int myGetStrDC(char *pBuf, int iBufLen, FILE *fd)
{
   char  sTmp[8192];
   bool  bDone=false;
   int   iRet, iTmp, iLen;

   if (!fd || !pBuf || iBufLen <= 0)
      return -1;

   *pBuf = 0;
   iLen = 0;
   do 
   {
      if (fgets(sTmp, 2048, fd))
      {
         iTmp = replNull(sTmp);
         iTmp = strlen(sTmp);
         iLen += iTmp;     // New output len
         if (iLen > iBufLen)
         {
            iRet = -2;
            bDone = true;
         } else
         {
            if (sTmp[iTmp-1] == '\n')
            {
               sTmp[iTmp-1] = 0;
               iLen--;
            }

            strcat(pBuf, sTmp);
            iRet = iLen;
            if (isdigit(sTmp[iRet-1]))
               bDone = true;
         }
      } else
      {
         bDone = true;
         iRet = 0;
      }
   } while (!bDone);

   return iRet;
}

/*****************************************************************************
 *
 * Desc: Get a string from a text file and check for number of token count.
 *       Read until get count tokens.
 *
 * 09/23/2015: call countChar() if delimiter is '|'
 * 10/16/2015: Fix bug by setting #tokens = countChar()+1 (SCR, BUT & SIS might affect the change)
 * 07/06/2019: call countChar() if delimiter is '~'
 * 08/07/2019: Add space after '.' or ','
 * 08/21/2019: Increase sTmp buffer to 65536
 *
 * Return number of bytes read.  <0 if read error
 *     0: EOF
 *    -1: File not open, invalid buffer pointer, invalid max buffer length
 *    -2: Buffer is not big enough
 *    -3: Number of tokens read > iCount
 *
 *****************************************************************************/

int myGetStrTC(char *pBuf, unsigned char cDelimiter, int iCount, int iBufLen, FILE *fd)
{
   char  sTmp[65536];
   bool  bDone=false;
   int   iRet, iTmp, iLen;

   if (!fd || !pBuf || iBufLen <= 0)
      return -1;

   *pBuf = 0;
   iLen = 0;
   do 
   {
      if (fgets(sTmp, 65536, fd))
      {
#ifdef _DEBUG
         //if (!memcmp(sTmp, "0113~282~18~0~000", 17))
         //   iTmp = 0;
#endif
         iTmp = replNull(sTmp);
         iTmp = strlen(sTmp);
         iLen += iTmp;     // New output len
         if (iLen > iBufLen)
         {
            iRet = -2;
            bDone = true;
         } else
         {
            if (sTmp[iTmp-1] == '\n')
            {
               sTmp[iTmp-1] = 0;
               iLen--;
            }

            // Add space after '.' or ','
            iTmp = strlen(pBuf);
            //if (*(pBuf+iTmp-1) == '.' || *(pBuf+iTmp-1) == ',')
            if (iTmp > 0 && *(pBuf+iTmp-1) != ' ' && sTmp[0] != ' ')
               strcat(pBuf, " ");
            strcat(pBuf, sTmp);

            if (cDelimiter == '|' || cDelimiter == '~')
               iRet = countChar(pBuf, cDelimiter)+1;
            else
               iRet = countTokens(pBuf, cDelimiter);
            if (iRet >= iCount)
               bDone = true;
         }
      } else
      {
         bDone = true;
         iRet = 0;
      }
   } while (!bDone);

   return iRet;
}

typedef wchar_t      FLD_NAME[20];
//typedef struct {
//   wchar_t name[20];
//   wchar_t type[1];
//   wchar_t value[50];
//} ZPTGFNFD;

#define sizeofU(var) sizeof(var)/2

void Get_Match_Docs(FLD_NAME *iFld_name1)
{
/*
   char     fld_name_buf1[21];
   int iTmp;
   iTmp = sizeofU(*iFld_name1);
   iTmp = sizeofU(iFld_name1);
   iTmp = sizeofU(FLD_NAME);
   iTmp = sizeof(ZPTGFNFD);
   iTmp = sizeofU(ZPTGFNFD);

   GetCharW(*iFld_name1, fld_name_buf1, sizeofU(*iFld_name1));
*/
}

void doTest(wchar_t *pszW, LPSTR pszA)
{
   /*
   HRESULT  lRet;
   int      iTmp, iSize;
   char     ansiBuf[19];
   wchar_t  wideBuf[50];
   FLD_NAME iFld_name1;

   wcscpy(iFld_name1, L"Name Test");
   Get_Match_Docs(&iFld_name1);

   iTmp = strlen(pszA);
   if (iTmp > 0)
   {
      // Convert ANSI to Wide character
      //lRet = AnsiToUnicode(pszA, wideBuf, iTmp);

      // Test SetCharW
      iSize = sizeof(wideBuf)/2;
      SetCharW(wideBuf, pszA, iSize);
      wcscpy(pszW, wideBuf);
   } else
   {
      iTmp = wcslen(pszW);
      if (iTmp > 0)
      {
         // Convert Wide character to ANSI
         //lRet = UnicodeToAnsi(pszW, ansiBuf, 100);
         iSize = sizeof(ansiBuf);
         GetCharW(pszW, ansiBuf, iSize);
         strcpy(pszA, ansiBuf);
      }
   }
   */
}

/******************************** mkpath() *********************************
 *
 * Creating full path folder.  This only works on local or map drive.  Drive 
 * letter is required.
 *
 ***************************************************************************/

int mkpath(char *pPath)
{
   char  *pTmp, *pTmp1, sPath[_MAX_PATH];
   int iRet=0;

   if (*(pPath+1) != ':')
      return -1;

   strcpy(sPath, pPath);
   pTmp1 = sPath;
   while (!iRet && (pTmp = strchr(pTmp1, '\\')))
   {
      *pTmp = 0;
      if (_access(sPath, 0))
         iRet = _mkdir(sPath);
      *pTmp = '\\';
      pTmp1 = ++pTmp;
   }

   if ((sPath[strlen(sPath)-1] != '\\') && _access(sPath, 0))
      iRet = _mkdir(sPath);

   return iRet;
}

/****************************** getFileDate() ******************************
 *
 * Return last modified file date
 *
 ***************************************************************************/

long getFileDate(LPCSTR pFilename, LPSTR timeStr)
{
   __int64  iTmp;
   long     lRet=0;
   char     acTmp[64];
   struct   _stati64 myStat;

   if (timeStr)
      *timeStr = 0;
   if (!_access(pFilename, 0))
   {
      iTmp = _stati64(pFilename, &myStat);
      dateString(acTmp, myStat.st_mtime);
      lRet = atol(acTmp);

      if (timeStr)
         strcpy(timeStr, acTmp);
   }

   return lRet;
}

__time64_t getFileDateTime(LPCSTR pFilename)
{
   __int64  iRet=0;
   struct   _stati64 myStat;

   if (!_access(pFilename, 0))
   {
      _stati64(pFilename, &myStat);
      iRet = myStat.st_mtime;
   }
   return iRet;
}

/****************************** getFileSize() ******************************
 *
 * Return file size.  If file not found, return -1;
 *
 ***************************************************************************/

__int64 getFileSize(LPCSTR pFilename)
{
   __int64   iRet=0;
   struct _stati64 sResult;

   if (!_access(pFilename, 0))
   {
      iRet = _stati64(pFilename, &sResult);
      if (!iRet)
         iRet = sResult.st_size;
   } else
      iRet = -1;
   return iRet;
}

/****************************** replEmbededChar ******************************
 *
 * Check file for null char embeded.  If output file provided, create output file.
 * Mode: -1=LT, 0=EQ, 1=GT
 *
 * Return 1 if char found, 0 if not.  Negative value if error occurs
 *
 *****************************************************************************/

int replEmbededChar(LPCSTR pInfile, LPCSTR pOutfile, char bSrchChar, char bReplChar, int iMode, int iRecLen)
{
   int      iTmp, iRet, iErrAt;
   long     lCnt=0;
   unsigned long   nBytesWritten, nBytesRead, iLen;
   BOOL     bRet;
   HANDLE   fhIn, fhOut;
   char     acBuf[8192];

   if (iRecLen <= 0 || iRecLen > 8192)
      iLen = 8192;
   else
      iLen = iRecLen;

   // Create output file if file name provided
   if (pOutfile && !_access(pOutfile, 0))
   {
      // Open Output file
      fhOut = CreateFile(pOutfile, GENERIC_WRITE, 0, NULL, CREATE_ALWAYS,
             FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

      if (fhOut == INVALID_HANDLE_VALUE)
         return -2;
   } else
      fhOut = 0;

   // Open input file
   fhIn = CreateFile(pInfile, GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING,
          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

   if (fhIn == INVALID_HANDLE_VALUE)
      return -1;

   iRet = 0;
   iErrAt = -1;
   bRet = true;
   while (bRet)
   {
      bRet = ReadFile(fhIn, acBuf, iLen, &nBytesRead, NULL);
      if (!bRet || !nBytesRead)
         break;

      iTmp = 0;
      while (iTmp < nBytesRead)
      {
         switch(iMode)
         {
            case 0:
               bRet = (acBuf[iTmp] == bSrchChar);
               break;
            case 1:
               bRet = (acBuf[iTmp] > bSrchChar);
               break;
            default:
               bRet = (acBuf[iTmp] < bSrchChar);
               break;
         }
         if (bRet)
         {
            iRet = 1;      
            if (fhOut)
            {
               if (iErrAt != lCnt)
               {
                  iErrAt = lCnt;
                  LogMsgD("***** Char <0x%2X> found at %d row %d", (unsigned char)acBuf[iTmp], iTmp, lCnt);
               }
               acBuf[iTmp] = bReplChar;
            } else
            {
               LogMsgD("***** Char <0x%2X> found at %d row %d", (unsigned char)acBuf[iTmp], iTmp, lCnt);
               break;
            }
         }
         iTmp++;
      }

      if (fhOut)
         bRet = WriteFile(fhOut, acBuf, nBytesRead, &nBytesWritten, NULL);
      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);
   }

   if (fhOut)
      CloseHandle(fhOut);
   if (fhIn)
      CloseHandle(fhIn);

   return iRet;
}

/*****************************************************************************
 *
 * Desc: Get a string from a text file and check for special first character and
 *       digit as the second byte to start a record.
 *
 * Return number of bytes read.  <0 if read error
 *     0: EOF
 *    -1: File not open, invalid buffer pointer, invalid max buffer length
 *    -2: Buffer is not big enough
 *
 *****************************************************************************/

int myGetStrBD(char *pBuf, char bChar, int iBufLen, FILE *fd)
{
   char  sTmp[2048];
   bool  bDone=false, bStart=true;
   int   iRet, iTmp, iLen;
   long  lCurPos;

   if (!fd || !pBuf || iBufLen <= 0)
      return -1;

   *pBuf = 0;
   iLen = 0;
   do 
   {
      lCurPos = ftell(fd);
      if (fgets(sTmp, 2048, fd))
      {
         if (!bStart && (sTmp[0] == bChar) && isdigit(sTmp[1]))
         {
            bDone = true;
            fseek(fd, lCurPos, SEEK_SET);
         }

         bStart = false;
         iTmp = strlen(sTmp);
         iLen += iTmp;     // New output len
         if (iLen > iBufLen)
         {
            iRet = -2;
            bDone = true;
         } else
         {
            if (sTmp[iTmp-1] == '\n')
            {
               sTmp[iTmp-1] = 0;
               iLen--;
            }

            strcat(pBuf, sTmp);
            iRet = iLen;
         }
      } else
      {
         bDone = true;
         iRet = 0;
      }
   } while (!bDone);

   return iRet;
}

/****************************************************************************
 *
 * Count number of records having the same date as specified
 *
 * Type: 1=year, 2=yyyymm, 3=yyyymmdd
 *
 ****************************************************************************/

int countDate(LPSTR pFilename, long lDate, long lOffset, long lType)
{
   FILE *fd;
   char  acRec[1024], *pTmp;
   long  lCnt, lTmp, lDone;

   lDone = lCnt = 0;
   fd = fopen(pFilename, "r");
   if (fd)
   {
      while (pTmp = fgets(acRec, 1024, fd))
      {
         if (lType == 1)
            lTmp = atoin(&acRec[lOffset], 4);
         else if (lType == 2)
            lTmp = atoin(&acRec[lOffset], 6);
         else if (lType == 3)
            lTmp = atoin(&acRec[lOffset], 8);

         if (lTmp == lDate)
            lDone++;

         if (lTmp > 2008 && acRec[lOffset+5] >= '5' && acRec[15] < '2')
            lTmp = 0;

         if (!(++lCnt % 10000))
            printf("\r%u", lCnt);
      }
   }

   return lDone;
}

/****************************************************************************
 *
 * Count number of token of a string.  Return number of tokens.
 *
 ****************************************************************************/

int countTokens(LPSTR pString, char cDelimiter)
{
   int i;
   int j = 1;
   int k;
   int InsideQuotes = 0;

   // Do not use myTrim() since it will wipe out <tab> at end of string
   if (cDelimiter != 9)
      myTrim(pString);
   k = (int)strlen(pString);

   for (i=0; i<k; i++)
   {                
      if (pString[i] == '\0' || pString[i] == 10)
         break;
      if (pString[i] == '\"')
         InsideQuotes = (InsideQuotes+1) % 2;
      if (InsideQuotes)
         continue;
      if (pString[i] == cDelimiter)
         j++;
   }

   return j;
}

/****************************************************************************
 *
 * Count number of token of specified record in a file
 *
 ****************************************************************************/

int countToken(LPCSTR pFilename, int iRecNum, char cDelimiter)
{
   FILE *fd;
   char  acRec[4096], *pTmp;
   int   iTmp, iCnt=-1;

   fd = fopen(pFilename, "r");
   if (fd)
   {
      for (iTmp = 0; iTmp < iRecNum; iTmp++)
      {
         pTmp = fgets(acRec, 4096, fd);
         if (!pTmp)
            break;
      }
      
      if (iTmp == iRecNum)
         iCnt = countChar(acRec, cDelimiter);

      fclose(fd);
   }

   return iCnt+1;
}

/****************************************************************************
 *
 * Count number of records in a file
 *
 ****************************************************************************/

int countLine(LPCSTR pFilename)
{
   FILE *fd;
   char  acRec[8192], *pTmp;
   int   iCnt=0;

   fd = fopen(pFilename, "r");
   while (fd)
   {
      pTmp = fgets(acRec, 4096, fd);
      if (!pTmp)
         break;
      iCnt++;
   }

   if (fd)
      fclose(fd);

   return iCnt;
}

/****************************************************************************
 *
 * Reset a character in the file for every line at specific place
 * Output the result to *.out
 *
 ****************************************************************************/

int resetOneChar(LPSTR pInfile, long lOffset, char bRepl)
{
   FILE *fdIn, *fdOut;
   char  sInrec[1024], sOutfile[_MAX_PATH], *pTmp;
   long  lCnt;

   strcpy(sOutfile, pInfile);
   pTmp = strrchr(sOutfile, '.');
   strcpy(pTmp, ".out");

   lCnt = 0;
   fdIn = fopen(pInfile, "r");
   fdOut = fopen(sOutfile, "w");
   if (fdIn)
   {
      fdOut = fopen(sOutfile, "w");
      while (pTmp = fgets(sInrec, 1024, fdIn))
      {
         sInrec[lOffset] = '0';
         fputs(sInrec, fdOut);

         if (!(++lCnt % 10000))
            printf("\r%u", lCnt);
      }

      fclose(fdOut);
      fclose(fdIn);
   }

   return lCnt;
}

/****************************************************************************
 *
 * Count number of occurences of a char in the string
 *
 ****************************************************************************/

int countChar(LPCSTR pBuf, char bChar)
{
   int iRet = 0;

   while (*pBuf)
      if (*pBuf++ == bChar)
         iRet++;

   return iRet;
}

/*****************************************************************************
 *
 * Get specified date based on today.  iDays > for future date and iDays < 0 for past date
 *
 ****************************************************************************/

int getExtendDate(char *pDate, int iDays)
{
   CTime today = CTime::GetCurrentTime();
   CTimeSpan tsExtend(iDays,0,0,0);
   today += tsExtend;

   if (pDate)
      strcpy(pDate, today.Format("%Y%m%d"));

   return atoi(today.Format("%Y%m%d"));
}

/*****************************************************************************
 *
 * Convert a word or a sentence to proper case.
 *
 ****************************************************************************/

CString toProperCase(CString& sWord)
{
   CString  sTmp;

   sTmp = sWord;
   sTmp.MakeLower();
   toupper(sTmp.GetAt(0));      

   return sTmp;
}

char *toProperCase(char *pWord, int iLen)
{
   char  *pTmp, *pResult;
   int   iTmp, iIdx;

   if (iLen > 0)
      iTmp = iLen;
   else
      iTmp = strlen(pWord);

   pTmp = pWord;
   pResult = pWord;
   *pResult++ = _toupper(*pTmp++);
   for (iIdx = 1; iIdx < iTmp; iIdx++)
      *pResult++ = _tolower(*pTmp++);

   return pWord;
}

/****************************** replBadChars ******************************
 * Replace unprintable char and back slash with space.
 **************************************************************************/

int replBadChar(unsigned char *pBuf, int iLen)
{
   int iTmp, iRet=0;

   for (iTmp = 0; iTmp < iLen; iTmp++, pBuf++)
   {
      if (*pBuf < ' ' || *pBuf == '\\' || (unsigned char)*pBuf > 126) 
      {
         *pBuf = ' ';
         iRet = iTmp;
      }
   }
   return iRet;
}

int replBadChars(char* pInfile, char *pOutfile, int iLen)
{
   FILE *fdInput, *fdOutput;
   unsigned char acTmp[8192];
   int  iRet, iCnt=0, iRecLen, iWriteLen, iFound=0;

   if (iLen > 0)
      iRecLen = iLen;
   else
      iRecLen = 8192;

   LogMsg("Check for bad chars in %s", pInfile);

   // Verify file open OK
   fdInput = fopen(pInfile, "rb");
   if (!fdInput)
   {
      LogMsg("***** Cannot open input file %s\n", pInfile);
      return -1;
   }

   fdOutput = fopen(pOutfile, "wb");
   if (!fdOutput)
   {
      fclose(fdInput);
      LogMsg("***** Cannot open output file %s\n", pOutfile);
      return -2;
   }

   // Start process
   while (!feof(fdInput))
   {
      iRet = fread(acTmp, 1, iRecLen, fdInput);
      if (!iRet)
         break;

      iWriteLen = iRet;

      // Ignore CRLF
      if (acTmp[iRet-1] == 10 || acTmp[iRet-1] == 13)
      {
         iRet--;
         if (acTmp[iRet-1] == 13)
            iRet--;
      }

      iRet = replBadChar(acTmp, iRet);
      if (iRet > 0)
         iFound++;

      // Wrire data to output file
      if (fdOutput)
         iRet = fwrite(acTmp, 1, iWriteLen, fdOutput);

      if (!(++iCnt%1000))
         printf("\r%ld", iCnt);
   }

   fclose(fdInput);
   if (fdOutput)
      fclose(fdOutput);

   printf("\n");
   LogMsg("Total record processed: %d", iCnt);
   LogMsg("Bad chars found:        %d\n", iFound);
   return iFound;
}

/*****************************************************************************
 *
 * Create an empty flag file
 *
 *****************************************************************************/

int CreateFlgFile(char *pCnty, char *pIniFile)
{
   FILE  *fd;
   char  sFlgFile[_MAX_PATH], sTmp[_MAX_PATH];
   int   iRet=0;

   GetIniString("Data", "FlgFile", "", sTmp, _MAX_PATH, pIniFile);
   sprintf(sFlgFile, sTmp, pCnty, pCnty);
   if (_access(sFlgFile, 0))
   {
      fd = fopen(sFlgFile, "w");
      if (fd)
         fclose(fd);
      else
         iRet = -1;
   }

   return iRet;
}

/*****************************************************************************
 *
 * Remove all normal files in temp folder
 *
 *****************************************************************************/

int RemoveTempFiles(char *pCnty, char *pTmpPath, bool bDelZip)
{
   char  sTmpFile[_MAX_PATH], sTmp[_MAX_PATH];
   int   iRet, iErrno=0;
   long  lHandle;
   struct _finddata_t sFileInfo;

   LogMsg("Remove temp files ...");
   sprintf(sTmp, "%s\\%s\\*.*", pTmpPath, pCnty);
   sFileInfo.attrib = _A_NORMAL;
   lHandle = _findfirst(sTmp, &sFileInfo);
   if (lHandle > 0)
   {
      do
      {
         if (sFileInfo.name[0] != '.' && (!bDelZip || !strstr(sFileInfo.name, ".zip")))
         {
            sprintf(sTmpFile, "%s\\%s\\%s", pTmpPath, pCnty, sFileInfo.name);
            iRet = sFileInfo.attrib & 0x00FF;
            if (iRet == _A_NORMAL || iRet == _A_ARCH)
            {
               LogMsg("--> Removing %s", sTmpFile);
               if (!DeleteFile(sTmpFile))
               {
                  LogMsg("***** Error removing temp file: %s", sTmpFile);
                  iErrno = errno;
                  break;
               }
            }
         }

         iRet = _findnext(lHandle, &sFileInfo);
      } while (!iRet);
      _findclose(lHandle);
   }

   return iErrno;
}

/*********************************** Check **********************************
 *
 ****************************************************************************/

int Check(bool bCorrect, LPCSTR pMsg)
{
   if ( ! bCorrect )
   {
      LogMsg0("***** Error: %s", pMsg);
      return 0;
   }
   return 1;
}

/******************************* RenameToExt ********************************
 *
 * Rename input file to provided extension
 *
 * Return 0 if success
 *
 ****************************************************************************/

int RenameToExt(LPSTR pFilename, LPSTR pExt)
{
   char  sTmp[_MAX_PATH], *pTmp;
   int   iRet=0;

   if (!_access(pFilename, 0))
   {
      strcpy(sTmp, pFilename);
      if (pTmp = strchr(sTmp, '.'))
         strcpy(pTmp+1, pExt);
      else
         sprintf(sTmp, "%s.%s", pFilename, pExt);

      if (!_access(sTmp, 0))
         iRet = DeleteFile(sTmp);

      // Rename this file
      iRet = rename(pFilename, sTmp);
   }

   return iRet;
}

/******************************* RebuildCsv ********************************
 *
 * Rebuild CSV file and validate that each row has a certain number of tokens.
 *
 * Input file must be a CSV file.
 *
 * Return number of output records if success, <0 if error.
 *
 ****************************************************************************/

int RebuildCsv(LPSTR pInfile, LPSTR pOutfile, unsigned char cDelim, int iTokens)
{
   int   iCnt=0, iRet;
   char  sBuf[65536];
   FILE  *fdInput, *fdOutput;

   LogMsg0("Rebuild CSV %s ==> %s", pInfile, pOutfile);

   // Verify file open OK
   fdInput = fopen(pInfile, "r");
   if (!fdInput)
   {
      LogMsg("***** Cannot open input file %s\n", pInfile);
      return -1;
   }

   fdOutput = fopen(pOutfile, "w");
   if (!fdOutput)
   {
      fclose(fdInput);
      LogMsg("***** Cannot open output file %s\n", pOutfile);
      return -2;
   }
            
   while (!feof(fdInput))
   {
#ifdef _DEBUG
      //if (!memcmp(sBuf, "044500070000", 9))    // 1403606..1406923
      //   iRet = 0;
#endif
      iRet = myGetStrTC(sBuf, cDelim, iTokens, 65536, fdInput);
      if (iRet > 0)
      {
         iRet = replChar(sBuf, 13, 32);
         strcat(sBuf, "\n");     // Put CRLF back on
         iRet = strlen(sBuf);
         if (iRet >= 65535)
            LogMsg("*** WARNING: RebuildCsv() has large string of %d characters.  Please check it out", iRet);
         fputs(sBuf, fdOutput);
      } else
      {
         if (iRet == -2)
            LogMsg("***** Buffer is too small: %.50s", sBuf);
         else if (iRet == -3)
            LogMsg("*** Too many tokens in row: %d", iCnt);
         break;
      }

      if (!(++iCnt % 1000))
         printf("\r%u", iCnt);
   }

   fclose(fdInput);
   fclose(fdOutput);

   LogMsg("RebuildCsv completed with %d records output.", iCnt);

   return iCnt;
}

/***************************** InitUtils *************************************
 *
 * Initialize m_sDataSrc & m_sMachine
 * To avoid infinite loop, do not use GetIniString() in InitUtils()
 *
 *****************************************************************************/

void InitUtils(LPCSTR pIniFile)
{
   strcpy(m_sMachine, getenv("COMPUTERNAME"));
   // If DataSrc not defined, default to local machine
   GetPrivateProfileString("System", "CO_DATA", m_sMachine, m_sDataSrc, 64, pIniFile);
   replStr(m_sDataSrc, "[Machine]", m_sMachine);

}

/***************************** GetIniString **********************************
 *
 * Get a string item from INI file and translate to appropriate machine path
 *
 *****************************************************************************/

int GetIniString(LPCSTR pSection, LPCSTR pItem, LPCSTR pDefVal, LPSTR pVal, int iMaxBuf, LPCSTR pIniFile)
{
   char  sTmp[1024];
   int   iRet;

   if (m_sMachine[0] <= ' ')
      InitUtils(pIniFile);

   iRet = GetPrivateProfileString(pSection, pItem, pDefVal, sTmp, iMaxBuf, pIniFile);
   if (iRet > 0)
   {
      if (!memcmp(sTmp, "[CO_DATA]", 9))
         iRet = replStr(sTmp, "[CO_DATA]", m_sDataSrc);
      else if (strstr(sTmp, "[Machine]"))
         iRet = replStr(sTmp, "[Machine]", m_sMachine);
      strcpy(pVal, sTmp);
      iRet = strlen(sTmp);
   } else
      *pVal = 0;

   return iRet;
}

/***************************** FixTxtFile ************************************
 *
 * Fix broken record file, use RebuildCsv
 *
 *****************************************************************************/

int FixTxtFile(LPCSTR pInfile, LPCSTR pOutfile, int iType)
{
   return 0;
}

/******************************* vmemcpy *************************************
 *
 * Copy upto specific number of bytes
 * Return number of bytes copied
 *
 *****************************************************************************/

int vmemcpy(LPSTR pDest, LPSTR pSrc, int iMaxLen, int iStrLen)
{
   int   iTmp;
   
   if (iStrLen <= 0)
      iTmp = strlen(pSrc);
   else
      iTmp = iStrLen;

   if (iTmp > iMaxLen)
      iTmp = iMaxLen;
   memcpy(pDest, pSrc, iTmp);
   return iTmp;
}

/******************************** SafeDelete() ********************************
 * Delete file by moving it recycle bin
 ******************************************************************************/

int SafeDelete(LPCSTR pFilepath)
{
   SHFILEOPSTRUCT shfos = {};
   shfos.hwnd   = nullptr;       // handle to window that will own generated windows, if applicable
   shfos.wFunc  = FO_DELETE;
   shfos.pFrom  = pFilepath;
   shfos.pTo    = nullptr;       // not used for deletion operations
   shfos.fFlags = FOF_ALLOWUNDO | FOF_NOCONFIRMATION | FOF_NOERRORUI | FOF_SILENT; // use the recycle bin

   int iRet;
   
   try
   {
      iRet = SHFileOperation(&shfos);
      if (iRet != 0)
         LogMsg("***** Error removing file: %s (0x%.x)", pFilepath, iRet);
   } catch (...)
   {
      LogMsg("***** Error removing file: %s (0x%.x)", pFilepath, iRet);
   }
   return iRet;
}

/************************************ GetExePath *******************************
 *
 * Find path of running program.
 *
 *******************************************************************************/

char *GetExePath(LPSTR pExePath, LPCSTR pArg0)
{
    char *pTmp, basePath[255] = "";

    _fullpath(basePath, pArg0, sizeof(basePath));

    if (pTmp = strrchr(basePath, '\\'))
    {
       *pTmp = 0;
       strcpy(pExePath, basePath);
    } else
       *pExePath = 0;

    return pExePath;
}

/************************************ _strstrn *********************************
 *
 * Find string within fix length string.
 *
 *******************************************************************************/

char *_strstrn(char *pString, char *pSearch, int iMaxLen)
{
   char  *pRet, sTmp[2048];
   int   iTmp;

   memcpy(sTmp, pString, iMaxLen);
   sTmp[iMaxLen] = 0;
   if (pRet = strstr(sTmp, pSearch))
   {
      iTmp = pRet - sTmp;
      pRet = pString+iTmp;
   }

   return pRet;
}

/******************************** myGetAppVersion *******************************
 *
 ********************************************************************************/

CString myGetAppVersion(LPSTR pBuffer)
{
   CString strResult;

   char szModPath[MAX_PATH];
   szModPath[ 0 ] = '\0';
   GetModuleFileName( NULL, szModPath, sizeof(szModPath) );
   DWORD dwHandle;
   DWORD dwSize = GetFileVersionInfoSize( szModPath, &dwHandle );

   if (dwSize > 0)
   {
      BYTE* pbBuf = static_cast<BYTE*>( alloca( dwSize ) );
      if (GetFileVersionInfo( szModPath, dwHandle, dwSize, pbBuf))
      {
         UINT uiSize;
         BYTE* lpb;
         if (VerQueryValue(pbBuf,
                              "\\VarFileInfo\\Translation",
                              (void**)&lpb,
                              &uiSize ) )
         {
            WORD* lpw = (WORD*)lpb;
            CString strQuery;
            //strQuery.Format( "\\StringFileInfo\\%04x%04x\\FileVersion", lpw[ 0 ], lpw[ 1 ] );
            strQuery.Format( "\\StringFileInfo\\%04x%04x\\ProductVersion", lpw[ 0 ], lpw[ 1 ] );
            if (VerQueryValue( pbBuf,
                              const_cast<LPSTR>( (LPCSTR)strQuery ),
                              (void**)&lpb,
                              &uiSize ) && uiSize > 0 )
            {
               strResult = (LPCSTR)lpb;
               if (pBuffer)
                  strcpy(pBuffer, (LPCSTR)lpb);
            }
         }
      }
   }

   return strResult;
}

int countWord(char *pStr)
{
   int   iRet=0;
   char  *pTmp1, *pTmp = myBTrim(pStr);

   if (!pTmp || !*pTmp)
      return 0;

   do {
       pTmp1 = strchr(pTmp, ' ');
       iRet++;
       pTmp = pTmp1+1;
   } while (pTmp1);

   return iRet;
}

/***************************** DayOfWeek ******************************
 *
 * Return day of the week: 1..7 --> Sunday .. Saturday
 *
 **********************************************************************/

int DayOfWeek(int iYear, int iMonth, int Day)
{
   CTime myTime(iYear, iMonth, Day, 1, 1, 1);
   int   iDayOfWeek;

   try
   {
      iDayOfWeek = myTime.GetDayOfWeek();
   } catch (...)
   {
      iDayOfWeek = 0;
   }

   return iDayOfWeek;
}

/********************************* remCrLf() ********************************
 *
 * Remove trailing CrLf
 * Return pointer to result string
 *
 ****************************************************************************/

char *remCrLf(char *pString)
{
   int i;

   i = strlen(pString)-1;
   if (i > 0 && pString[i] < ' ')
   {
      pString[i] = 0;
      if (i > 1 && pString[i-1] < ' ')
         pString[i-1] = 0;
   }
   return pString;
}

/********************************* Zone2Dbl *********************************
 *
 * Convert zone decimal to double
 * Return decimal value
 *
 ****************************************************************************/

double Zone2Dbl(char *pZoneValue, int iLen, double dDiv)
{
   char     sTmp[32], cChkVal;
   double   dRet;
   bool     bPositive=true;

   memcpy(sTmp, pZoneValue, iLen);
   cChkVal = sTmp[iLen-1];
   if (cChkVal >= 'A' &&  cChkVal <= 'I')
      sTmp[iLen-1] = (cChkVal & 0x0F) + 48;
   else if (cChkVal >= 'J' &&  cChkVal <= 'R')
   {
      bPositive = false;
      sTmp[iLen-1] = ((cChkVal-9) & 0x0F) + 48;
   } else
   {
      sTmp[iLen-1] = '0';
      if (cChkVal == '}')
         bPositive = false;
   }

   sTmp[iLen] = 0;
   dRet = (double)atol(sTmp)/dDiv;

   if (!bPositive)
      dRet = -dRet;
   return dRet;
}

/********************************* Zone2Int *********************************
 *
 * Convert zone decimal to integer
 * Return int value
 *
 ****************************************************************************/

int Zone2Int(char *pZoneValue, int iLen)
{
   char     sTmp[32], cChkVal;
   int      iRet;
   bool     bPositive=true;

   memcpy(sTmp, pZoneValue, iLen);
   cChkVal = sTmp[iLen-1];
   if (cChkVal >= 'A' &&  cChkVal <= 'I')
      sTmp[iLen-1] = (cChkVal & 0x0F) + 48;
   else if (cChkVal >= 'J' &&  cChkVal <= 'R')
   {
      bPositive = false;
      sTmp[iLen-1] = ((cChkVal-9) & 0x0F) + 48;
   } else
   {
      sTmp[iLen-1] = '0';
      if (cChkVal == '}')
         bPositive = false;
   }

   sTmp[iLen] = 0;
   iRet = atoi(sTmp);

   if (!bPositive)
      iRet = -iRet;
   return iRet;
}

/******************************************************************************
 *
 * Compare file date.  If file1 is newer, return 1.  If file2 is newer, return 2.
 * If they are the same, return 0.
 * On error, return -1 or -2 or -3;
 *
 ******************************************************************************/

int chkFileDate(LPCSTR pFile1, LPCSTR pFile2)
{
   long  lFile1, lFile2;
   int   iRet = 0;

   lFile1 = getFileDate(pFile1);
   lFile2 = getFileDate(pFile2);

   if (!lFile1 && !lFile2)
      iRet = -3;
   else if (!lFile1)
      iRet = -1;
   else if (!lFile2)
      iRet = -2;
   else if (lFile1 > lFile2)
      iRet = 1;
   else if (lFile2 > lFile1)
      iRet = 2;
   else 
      iRet = 0;

   return iRet;
}

int chkFileDateTime(LPCSTR pFile1, LPCSTR pFile2)
{
   __int64  lFile1, lFile2;
   int      iRet = 0;

   lFile1 = getFileDateTime(pFile1);
   lFile2 = getFileDateTime(pFile2);

   if (!lFile1)
      iRet = -1;
   else if (!lFile2)
      iRet = -2;
   else if (lFile1 > lFile2)
      iRet = 1;
   else if (lFile2 > lFile1)
      iRet = 2;
   else 
      iRet = 0;

   return iRet;
}

/******************************* ChkBlankRow *********************************
 *
 * Check each line for blank at specific position (default 0)
 * Output row start with blank
 * Return number of bad records output
 *
 *****************************************************************************/

int ChkBlankRow(LPCSTR pInFile, LPCSTR pOutFile, int iOffset /* zero base */)
{
   char     acBuf[2048], acTmp[32];
   long     lCnt=0, lOut=0;
	FILE		*fdOut, *fdIn;

   LogMsg("\nCheck blank APN for %s", pInFile);

   if (!(fdOut = fopen(pOutFile, "w")))
   {
      LogMsg("***** Error creating output file: %s (%d)", pOutFile, _errno);
      return -1;
   }

   // Open input file
   if (!(fdIn = fopen(pInFile, "r")))
   {
      LogMsg("***** Error opening %s file (%d)", pInFile, _errno);
      return -1;
   }

   // Loop through
   while (!feof(fdIn))
   {
      // Get input rec
      if (!fgets(acBuf, 2048, fdIn))
         break;         // EOF

      if (acBuf[iOffset] == ' ')
		{
         sprintf(acTmp, "%d:", lCnt);
			fputs(acTmp, fdOut);
			fputs(acBuf, fdOut);
			lOut++;
		}

      if (!(++lCnt % 10000))
         printf("\r%u", lCnt);
   }

   fclose(fdIn);
   fclose(fdOut);

   LogMsg("Number of records processed:    %ld", lCnt);
   LogMsg("Number of records /w blank APN: %ld\n", lOut);
   return lOut;
}

/********************************* AddString *********************************
 *
 * Add delimiter to output string with option to add before or after
 *
 *****************************************************************************/

char *AddString(char *pOutStr, char *pInStr, char *pDelim, bool bAfter)
{
	if (bAfter)
		sprintf(pOutStr, "%s%s", pInStr, pDelim);
	else
		sprintf(pOutStr, "%s%s", pDelim, pInStr);
	return pOutStr;
}

/******************************* AppendString ********************************
 *
 *
 *****************************************************************************/

void AppendString(char *pOutStr, char *pInStr, char *pDelim, bool bAfter)
{
	char	acTmp[4096];

	if (bAfter)
		sprintf(acTmp, "%s%s", pInStr, pDelim);
	else
		sprintf(acTmp, "%s%s", pDelim, pInStr);

	strcat(pOutStr, acTmp);
}

/******************************* FindLatestFile ******************************
 *
 * Find a specified file in subfolder.  This is specific for MPA where tax
 * subfolder is in the format of "MMM YYYY" i.e. "Jul 2017".  Use with caution.
 *
 *****************************************************************************/

char *FindLatestFile(char *pSrcDir, char *pFileName, char *pDstDir)
{
   char *pRet = NULL;
   char  acCurDir[MAX_PATH], acCurFile[MAX_PATH];

   long     lRet, lHandle;
   int      iLastMonth=1, iLastYear=2016, iMonth, iYear;
   struct   _finddata_t  sFileInfo;

   sprintf(acCurDir, "%s\\*", pSrcDir);
   lHandle = _findfirst(acCurDir, &sFileInfo);
   if (lHandle < 0)
      return pRet;

   // Find latest sub folder
   acCurDir[0] = 0;
   do
   {
      lRet = _findnext(lHandle, &sFileInfo);
      if (lHandle > 0)
      {
         if (sFileInfo.attrib & _A_SUBDIR && sFileInfo.name[0] != '.')
         {
            for (iMonth = 0; iMonth < 12; iMonth++)
               if (!_memicmp(sFileInfo.name, asMonth[iMonth], 3))
                  break;
            if (iMonth < 12)
            {
               iMonth++;
               iYear = atol(&sFileInfo.name[4]);
               if (iYear > iLastYear || (iYear == iLastYear && iMonth > iLastMonth))
               {
                  sprintf(acCurFile, "%s\\%s\\%s", pSrcDir, sFileInfo.name, pFileName);
                  if (!_access(acCurFile, 0))
                  {
                     iLastYear = iYear;
                     iLastMonth = iMonth;
                     sprintf(acCurDir, "%s\\%s", pSrcDir, sFileInfo.name);
                  } 
               }
            }
         } 
      }
   } while (!lRet);
   _findclose(lHandle);

   if (acCurDir[0])
      pRet = strcpy(pDstDir, acCurDir);
   return pRet;
}

/******************************** LaunchApp **********************************
 *
 * Execute an application and wait for the app to complete.
 *
 * Return 0 if successful, Otherwise error
 *
 *****************************************************************************/

int LaunchApp(char *pApp, char *pArgv)
{
   int iRet;

   STARTUPINFO StartupInfo;
   PROCESS_INFORMATION ProcessInfo;

   memset(&StartupInfo, 0, sizeof(STARTUPINFO));
   memset(&ProcessInfo, 0, sizeof(PROCESS_INFORMATION));

   iRet = CreateProcessA(pApp, pArgv,
                   NULL, NULL, false, NULL, NULL, NULL,
                   &StartupInfo,
                   &ProcessInfo
   );

   if (iRet)
   {
      WaitForSingleObject(ProcessInfo.hProcess, 600000);    // Wait for 10 mins

      // Get the exit code.
      //DWORD exitCode;
      //iRet = GetExitCodeProcess(ProcessInfo.hProcess, &exitCode);

      CloseHandle(ProcessInfo.hProcess);
      CloseHandle(ProcessInfo.hThread);
      iRet = 0;
   }
   return iRet;
}

/*
 Executes the given command using CreateProcess() and WaitForSingleObject().
 Returns FALSE if the command could not be executed or if the exit code could not be determined.
*/

BOOL executeCommandLine(CString cmdLine, DWORD & exitCode)
{
   PROCESS_INFORMATION processInformation = {0};
   STARTUPINFO startupInfo                = {0};
   startupInfo.cb                         = sizeof(startupInfo);
   int nStrBuffer                         = cmdLine.GetLength() + 50;


   // Create the process
   BOOL result = CreateProcess(NULL, cmdLine.GetBuffer(nStrBuffer), 
                               NULL, NULL, FALSE, 
                               NORMAL_PRIORITY_CLASS | CREATE_NO_WINDOW, 
                               NULL, NULL, &startupInfo, &processInformation);
   cmdLine.ReleaseBuffer();


   if (!result)
   {
      // CreateProcess() failed
      // Get the error from the system
      LPVOID lpMsgBuf;
      DWORD dw = GetLastError();
      FormatMessage(FORMAT_MESSAGE_ALLOCATE_BUFFER | FORMAT_MESSAGE_FROM_SYSTEM | FORMAT_MESSAGE_IGNORE_INSERTS, 
                    NULL, dw, MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT), (LPTSTR) &lpMsgBuf, 0, NULL);

      // Display the error
      CString strError = (LPTSTR) lpMsgBuf;
      TRACE(_T("::executeCommandLine() failed at CreateProcess()\nCommand=%s\nMessage=%s\n\n"), cmdLine, strError);

      // Free resources created by the system
      LocalFree(lpMsgBuf);

      // We failed.
      return FALSE;
   }
   else
   {
      // Successfully created the process.  Wait for it to finish.
      WaitForSingleObject( processInformation.hProcess, INFINITE );

      // Get the exit code.
      result = GetExitCodeProcess(processInformation.hProcess, &exitCode);

      // Close the handles.
      CloseHandle( processInformation.hProcess );
      CloseHandle( processInformation.hThread );

      if (!result)
      {
         // Could not get exit code.
         TRACE(_T("Executed command but couldn't get exit code.\nCommand=%s\n"), cmdLine);
         return FALSE;
      }

      // We succeeded.
      return TRUE;
   }
}

/******************************************************************************
 *
 * Search for specific word in first row.  If found, return 1, return 0 if not
 * If bad file, return -1;
 *
 ******************************************************************************/

int chkFileStart(LPSTR pInFile, LPSTR pWord, int iStartAt)
{
   char     acBuf[4096];
	FILE		*fdIn;
   int      iRet = 0;

   LogMsg("\nCheck %s", pInFile);

   // Open input file
   if (!(fdIn = fopen(pInFile, "r")))
   {
      LogMsg("***** Error opening %s file (%d)", pInFile, _errno);
      return -1;
   }

   // Get first rec
   if (fgets(acBuf, 4096, fdIn))
   {
      if (!_memicmp(&acBuf[iStartAt], pWord, strlen(pWord)))
         iRet = 1;
   } else
      iRet = -1;

   fclose(fdIn);

   return iRet;
}

/****************************** chkSignValue *********************************
 *
 * Check for sign byte and change to appropriate value then return true if negative
 * Last digit translation are: }=0, {=0
 * Negative value: J=1,K=2,L=3,M=4,N=5,O=6,P=7,Q=8,R=9
 * Positive value: A=1,B=2,C=3,D=4,E=5,F=6,G=7,H=8,I=9
 * Return true if negative, false if positive
 *
 *****************************************************************************/

bool chkSignValue(char *pValue, int iSize)
{
   char cTmp;
   bool bRet = false;

   cTmp = *(pValue+iSize-1);
   if (cTmp == '}' || cTmp == '{')
      *(pValue+iSize-1) = '0';
   else if (cTmp >= 'A' && cTmp <= 'I')
   {
      *(pValue+iSize-1) = cTmp - 0x10;
      bRet = false;
   } else if (cTmp >= 'J' && cTmp <= 'R')
   {
      *(pValue+iSize-1) = cTmp - 0x19;
      bRet = true;
   } 

   return bRet;
}

/********************************** strRight *********************************
 *
 * Copy number of chars from the right
 *
 *****************************************************************************/

char *strRight(char *pInput, int iCnt, char *pOutput)
{
   char *pRet;
   int   iTmp;

   if (!pInput)
      return pInput;

   iTmp = strlen(pInput);
   if (iTmp > iCnt)
   {
      pRet = pInput+(iTmp - iCnt);
   } else
      pRet = pInput;

   if (pOutput)
      strcpy(pOutput, pRet);

   return pRet;
}

/******************************* FixDollarField ******************************
 *
 * This function is used to fix comma delimited record by removing extra comma
 * in dollar field starts with dollar sign '$'.
 *
 * Search for '$'. When found, look for '.' before a comma
 * Return number of records fixed.
 *
 *****************************************************************************/

int FixDollarField(LPSTR pInfile, LPSTR pOutfile)
{
   int   iCnt=0, iFixed=0;
   char  sBuf[65536], *pRec, *pComma, *pDollar, *pDot;
   FILE  *fdInput, *fdOutput;

   LogMsg0("Fix dollar amount %s ==> %s", pInfile, pOutfile);

   // Verify file open OK
   fdInput = fopen(pInfile, "r");
   if (!fdInput)
   {
      LogMsg("***** Cannot open input file %s\n", pInfile);
      return -1;
   }

   fdOutput = fopen(pOutfile, "w");
   if (!fdOutput)
   {
      fclose(fdInput);
      LogMsg("***** Cannot open output file %s\n", pOutfile);
      return -2;
   }
            
   while (!feof(fdInput))
   {
      pRec = fgets(sBuf, 65535, fdInput);
      while (pRec)
      {
         if (pDollar = strchr(pRec, '$'))
         {
            pDot = strchr(pDollar, '.');
            pComma = strchr(pDollar, ',');
            if (pComma < pDot)
            {
               pRec = pComma;
               // Remove comma
               do {
                  *pRec++ = *(++pComma);
               } while (*pComma != ',');
               *pRec++ = ' ';
               iFixed++;
            } else
               pRec = pComma;
         } else
            pRec = NULL;
      }

      fputs(sBuf, fdOutput);

      if (!(++iCnt % 1000))
         printf("\r%u", iCnt);
   }

   fclose(fdInput);
   fclose(fdOutput);

   LogMsg("Number of records processed: %d", iCnt);
   LogMsg("Number of records fixed:     %d\n", iFixed);

   return iFixed;
}
